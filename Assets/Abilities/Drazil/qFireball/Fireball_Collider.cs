﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball_Collider : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.GetComponent<IReceiver>() != null)
        {
            transform.parent.GetComponent<Fireball>().ProjectileHit(other.transform.GetComponent<IReceiver>());
        }
    }
}