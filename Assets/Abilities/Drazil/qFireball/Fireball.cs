﻿using UnityEngine;
using UnityEngine.Networking;

public class Fireball : Spell {

    public GameObject _actualFireball;

    [Header("Balancing")]
    public float _range;
    public float _speed;
    public float _damage;
    public float _dot_Duration;
    public float _dot_TotalDamage;
    public float _dot_tick;

    private Vector3 _direction;
    private float _duration;

    protected override void Execute()
    {
        _actualFireball.SetActive(true);
        _direction = new Vector3(_mousePositionAtCast.x - _casterPositionAtCast.x, 0.0f, _mousePositionAtCast.z - _casterPositionAtCast.z).normalized;
        _actualFireball.transform.localPosition = new Vector3(0.0f, 1.0f, 0.0f);
        _actualFireball.transform.forward = _direction;
        _duration = _range / _speed;
    }
    public override void AllUpdate()
    {
        if (_networkStartTime + _duration > Network.time)
        {
            _actualFireball.transform.position = transform.position + (((float)Network.time - _networkStartTime) * _direction * _speed);
            _actualFireball.transform.position = new Vector3(_actualFireball.transform.position.x, 1.0f, _actualFireball.transform.position.z);
        }
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + _duration <= Network.time)
        {
            NetworkServer.Destroy(gameObject);
        }
    }

    public void ProjectileHit(IReceiver rec)
    {
        if(isServer)
        {
            if(rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, _damage);
                rec.DamageOverTime(_caster.DamageMultiplier, _dot_TotalDamage, _dot_Duration, _dot_tick);
                NetworkServer.Destroy(gameObject);
            }
        }
    }
}
