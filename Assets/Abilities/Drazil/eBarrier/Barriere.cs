﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Barriere : Spell {

    public GameObject _BarrierObject;

    [Header("Balancing")]
    public float _radius;
    public float _durationSec;
    public float _slowPercentage;
    public float _shieldArmorAmount;
    public float _tick;

    private float _tickTimer;
    private Vector3 _spherePosition;
    private int _casterTeam;

    protected override void Execute()
    {
        float distanceToSpawnPosition = (_mousePositionAtCast - _casterPositionAtCast).magnitude;
        
        _spherePosition = _mousePositionAtCast;
        _BarrierObject.transform.position = _spherePosition;
        _tickTimer = _networkStartTime;
        if(isServer)
            _casterTeam = _caster.GetTeam();
    }

    public override void AllUpdate()
    {
    }

    public override void ServerUpdate()
    {
        if(_networkStartTime+_durationSec >= Network.time)
        {
            if(Network.time-_tickTimer >= _tick)//Übersprungene ticks aufholen
            {
                Collider[] colliders = Physics.OverlapSphere(_spherePosition,_radius);
                IReceiver _temp = null;
                foreach(Collider c in colliders)
                {
                    _temp = c.GetComponent<IReceiver>();
                    if (_temp != null)
                    {
                        Debug.Log("Got IReciever: " + _temp.GetTeamAndMemberHash());
                        if(_temp.GetTeam() == _casterTeam)
                        {
                            //TODO noch nicht implementiert _temp.GiveArmorShield(_shieldArmorAmount, _tick);
                            _temp.SpeedUp(100, _tick);
                        }
                        else
                        {
                            _temp.Slow(_slowPercentage, _tick);
                        }
                    }
                }
                _tickTimer += _tick;
            }
        }
        else
        {
            NetworkServer.Destroy(gameObject); 
        }
    }
}
