﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Meteorshower : Spell {

    public GameObject _staticParent;
    public GameObject _movingParent;
    
    [Header("Balancing")]
    public float _speed;
    public float _damagePerHit;
    public float _damagePerTick;
    public float _tickDuration;


    private float _goalY = -400.0f;
    private float _endTime;
    private float _curTick;

    protected override void Execute()
    {
        _staticParent.transform.position = new Vector3(_mousePositionAtCast.x, 0.0f, _mousePositionAtCast.z);
        _endTime = _networkStartTime + Mathf.Abs(_goalY/_speed);
        _curTick = -0.5f * _tickDuration;

    }

    public override void AllUpdate()
    {
        _movingParent.transform.position = new Vector3(_staticParent.transform.position.x, Mathf.InverseLerp(_networkStartTime, _endTime, (float)Network.time) * _goalY, _staticParent.transform.position.z);
    }

    public override void ServerUpdate()
    {
        if (Network.time >= _endTime)
            NetworkServer.Destroy(gameObject);
        else if(_networkStartTime+_curTick < Network.time)
        {
            _curTick += _tickDuration;
            Collider[] col = Physics.OverlapSphere(_mousePositionAtCast, 10.0f);
            IReceiver rec = null;
            foreach(Collider c in col)
            {
                rec = c.GetComponent<IReceiver>();
                if(rec!= null)
                {
                    if(rec.GetTeam() != _caster.GetTeam())
                        rec.Damage(_caster.DamageMultiplier, _damagePerTick);
                }
            }
        }
    }

    public void MeteorHit(Collider hit)
    {
        if(isServer)
        {
            IReceiver rec = hit.GetComponent<IReceiver>();
            if(rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, _damagePerHit);
            }
        }
    }
}
