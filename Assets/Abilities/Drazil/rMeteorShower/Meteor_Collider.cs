﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor_Collider : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IReceiver>() != null)
        {
            GetComponentInParent<Meteorshower>().MeteorHit(other);
        }
    }

}
