﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ChainLightning : Spell
{
    public GameObject _actualBolt1;
    public GameObject[] _boltsPhase2;

    [Header("Balancing")]
    public float _range;
    public float _speed;
    public float _rootDuration;

    private float _duration;

    private int _phase;//1 = erster blitz unterwegs oder hat getroffen //2 = blitze der 2. phase haben getroffen / sind unterwegs
    private Vector3 _direction1;


    private float _secondPhaseStartTime;
    private int _secondPhaseBoltCount;
    private Vector3[] _secondPhaseDirections;
    private bool[] _boltStilActive;
    private Vector3 _secondPhaseStartPosition;

    private List<int> _playersHit;

    protected override void Execute()
    {
        _phase = 1;
        _duration = _range / _speed;
        _actualBolt1.SetActive(true);
        _direction1 = new Vector3(_mousePositionAtCast.x - _casterPositionAtCast.x, 0.0f, _mousePositionAtCast.z - _casterPositionAtCast.z).normalized;

        _actualBolt1.transform.position = new Vector3(_casterPositionAtCast.x, 1.0f, _casterPositionAtCast.z);
        _actualBolt1.transform.up = -_direction1;

        if (isServer)
        {
            _playersHit = new List<int>();
        }

    }

    Vector3 newPos;
    public override void AllUpdate()
    {
        if (_networkStartTime + _duration >= Network.time && _phase == 1
            || _secondPhaseStartTime+_duration >= Network.time && _phase == 2)
        {
            if (_phase == 1)
            {
                newPos = _casterPositionAtCast + (_direction1 * _speed * ((float)Network.time - _networkStartTime));
                newPos.y = 1.0f;
                _actualBolt1.transform.position = newPos;
            }
            else if (_phase == 2)
            {
                for(int i = 0; i < _secondPhaseBoltCount; ++i)
                {
                    if (_boltStilActive[i])
                    {
                        _boltsPhase2[i].transform.position = _secondPhaseStartPosition + (_secondPhaseDirections[i] * _speed * ((float)Network.time - _secondPhaseStartTime));
                    }
                }
            }
        }
        else
        {
            if(_phase == 1)
            {
                _actualBolt1.SetActive(false);
            }
            else if(_phase == 2)
            {
                for(int i = 0; i < _secondPhaseBoltCount; ++i)
                {
                    if(_boltStilActive[i])
                    {
                        _boltStilActive[i] = false;
                        _boltsPhase2[i].SetActive(false);
                    }
                }
            }
        }
    }

    public override void ServerUpdate()
    {
        if(_networkStartTime+_duration < Network.time && _phase == 1
            || _secondPhaseStartTime+_duration < Network.time && _phase == 2)
        {
            NetworkServer.Destroy(gameObject);
        }
    }

    [ClientRpc]
    public void RpcNextTarget(Vector3 startPosition, Vector3[] targetDirections, float NetworkStartTime)
    {
        NextTargets(startPosition, targetDirections, NetworkStartTime);
    }

    private void NextTargets(Vector3 startPosition, Vector3[] targetDirections, float NetworkStartTime)
    {
        _phase++;
        _actualBolt1.SetActive(false);
        _secondPhaseDirections = targetDirections;
        _secondPhaseStartTime = NetworkStartTime;
        _secondPhaseStartPosition = startPosition;
        _secondPhaseBoltCount = targetDirections.Length;
        _boltStilActive = new bool[targetDirections.Length];
        for(int i = 0; i < _secondPhaseBoltCount; ++i)
        {
            _boltStilActive[i] = true;
            _boltsPhase2[i].SetActive(true);
            _boltsPhase2[i].transform.position = _secondPhaseStartPosition;
            _boltsPhase2[i].transform.up = -_secondPhaseDirections[i];
            
        }
    }

    [ClientRpc]
    public void RpcBoltSecondPhaseHit(int iNumber)
    {
        BoltSecondPhaseHit(iNumber);
    }

    private void BoltSecondPhaseHit(int iNumber)
    {
        Debug.Log("BoltSecondPhaseHit");
        _boltsPhase2[iNumber - 1].SetActive(false);
        _boltStilActive[iNumber - 1] = false;
    }

    public void OnColliderHit(Collider hit, int iNumber)
    {
        if(isServer)
        {
            IReceiver receiver = hit.GetComponent<IReceiver>();
            IReceiver _tempOtherPlayer = null;
            if (receiver.GetTeam() != _caster.GetTeam())
            {
                if (_phase == 1)
                {
                    Debug.Log("ChainLightning.OnColliderHit phase 1");
                    _playersHit.Add(receiver.GetTeamAndMemberHash());
                    //TODO receiver.Root(_rootDuration);//TODO hier effekt hinzufügen
                    receiver.Damage(_caster.DamageMultiplier, 10);//TODO später löschen
                    Collider[] colliders = Physics.OverlapSphere(hit.transform.position, _range);
                    List<Collider> nextStep = new List<Collider>();
                    int iCurObject = 0;
                    for(int i = 0; i < colliders.Length && iCurObject < 3; ++i)
                    {
                        if(colliders[i].GetComponent<IReceiver>() != null)
                        {
                            _tempOtherPlayer = colliders[i].GetComponent<IReceiver>();
                            if(_tempOtherPlayer.GetTeam() != _caster.GetTeam() 
                                && _tempOtherPlayer.GetTeamAndMemberHash() != receiver.GetTeamAndMemberHash())
                            {
                                nextStep.Add(colliders[i]);
                                iCurObject++;
                            }
                        }
                    }
                    if(nextStep.Count != 0)
                    {
                        Vector3[] nextDirections = new Vector3[nextStep.Count];
                        Vector3 startPosition = hit.transform.position;
                        startPosition.y = 1.0f;
                        for(int i = 0; i < nextStep.Count; ++i)
                        {
                            nextDirections[i] = (nextStep[i].transform.position-startPosition);
                            nextDirections[i].y = 0.0f;
                            nextDirections[i].Normalize();
                        }
                        float startTime = (float)Network.time;
                        RpcNextTarget(startPosition, nextDirections, startTime);
                        NextTargets(startPosition, nextDirections, startTime);
                    }
                    else
                    {
                        NetworkServer.Destroy(gameObject);
                    }
                }
                else if (_phase == 2)
                {
                    if (!_playersHit.Contains(receiver.GetTeamAndMemberHash()))
                    {
                        //TODO receiver.Root(_rootDuration);//TODO hier effekte hinzufügen
                        receiver.Damage(_caster.DamageMultiplier, 20);//TODO löschen
                        RpcBoltSecondPhaseHit(iNumber);
                        BoltSecondPhaseHit(iNumber);
                        _playersHit.Add(receiver.GetTeamAndMemberHash());
                    }
                }
            }
        }
    }
}