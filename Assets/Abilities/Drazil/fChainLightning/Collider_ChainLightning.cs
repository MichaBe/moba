﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collider_ChainLightning : MonoBehaviour {

    public ChainLightning _parent;
    public int iNumber;

    public void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IReceiver>() != null)
            _parent.OnColliderHit(other, iNumber);
    }
}
