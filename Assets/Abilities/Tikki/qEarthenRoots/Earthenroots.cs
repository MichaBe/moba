﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

public class Earthenroots : Spell
{
    public float Duration;
    public float Radius;
    public GameObject RootPrefab;

    protected override void Execute()
    {
        Debug.Log("Earthen Roots gecasted");
        RootPrefab.SetActive(false);

        if (!isServer)
            return;

        Collider[] colliders = Physics.OverlapSphere(_mousePositionAtCast, Radius);

        if (colliders.Length <= 0)
            return;

        foreach (var c in colliders)
        {
            CharacterLoader temp = c.GetComponent<CharacterLoader>();

            if (!temp || temp.GetTeam() == _caster.GetTeam())
                continue;

            temp.Root(Duration);
            RpcActivateRoot(temp.transform.position);
        }
    }

    public override void AllUpdate()
    {
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + Duration <= Network.time)
            NetworkServer.Destroy(gameObject);
    }

    [ClientRpc]
    public void RpcActivateRoot(Vector3 position)
    {
        RootPrefab.transform.position = position;
        RootPrefab.SetActive(true);
    }
}
