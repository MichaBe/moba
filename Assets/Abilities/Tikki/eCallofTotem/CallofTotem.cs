﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CallofTotem : Spell {

    public float Duration;
    public float Radius;
    public float Tick;
    public float Heal;

    public GameObject TotemPrefab;
    public GameObject AoeHealPrefab;

    private float _tickTimer;
    private Vector3 _totemPosition;
    private int _casterTeam;

    protected override void Execute()
    {
        TotemPrefab.transform.position = _mousePositionAtCast;
        AoeHealPrefab.transform.position = new Vector3(_mousePositionAtCast.x, 0.2f, _mousePositionAtCast.z);
        AoeHealPrefab.GetComponent<SpriteRenderer>().size = new Vector2(Radius * 2, Radius * 2);
        _totemPosition = _mousePositionAtCast;
        _tickTimer = _networkStartTime;
        if (isServer)
            _casterTeam = _caster.GetTeam();
    }

    public override void AllUpdate()
    {
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + Duration <= Network.time)
        {
            NetworkServer.Destroy(gameObject);
            return;
        }

        //Übersprungene ticks aufholen
        if (Network.time - _tickTimer >= Tick)
        {
            Collider[] colliders = Physics.OverlapSphere(_totemPosition, Radius);
            foreach (Collider c in colliders)
            {
                var temp = c.GetComponent<IReceiver>();
                if (temp == null || temp.GetTeam() != _casterTeam)
                    continue;

                temp.Heal(Heal);
            }
            _tickTimer += Tick;
        }
    }
}
