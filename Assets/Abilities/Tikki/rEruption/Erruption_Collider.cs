﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Erruption_Collider : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IReceiver>() != null)
        {
            GetComponentInParent<Erruption>().ErruptionHit(other);
        }
    }

}
