﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Erruption : Spell {

    public float Duration;
    public float Radius;
    public float Tick;
    public float DamagePerHit;

    public GameObject VolcanoPrefab;
    public GameObject FirePrefab;
    public GameObject TargetIndicator;

    private float _tickTimer;

    private Vector3 _volcanoPosition;

    protected override void Execute()
    {
        VolcanoPrefab.transform.position = _mousePositionAtCast;
        _volcanoPosition = _mousePositionAtCast;
        _tickTimer = _networkStartTime;
    }

    public override void AllUpdate()
    {
        if (_networkStartTime + Duration <= Network.time)
            return;

        if (Network.time - _tickTimer >= Tick)
        {
            GameObject fire = Instantiate(FirePrefab, VolcanoPrefab.transform);
            GameObject target = Instantiate(TargetIndicator, VolcanoPrefab.transform);
            Vector3 targetPosition = Position.RandomCircumferencePoint(_volcanoPosition, Random.Range(5.0f, Radius));
            target.GetComponent<SpriteRenderer>().size = new Vector2(fire.GetComponent<SphereCollider>().radius, fire.GetComponent<SphereCollider>().radius);
            target.transform.position = new Vector3(targetPosition.x, 0.2f, targetPosition.z);
            Position.JumpToPositionWithAngle(fire, 80.0f, targetPosition);
            _tickTimer += Tick;
        }
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + Duration <= Network.time)
            NetworkServer.Destroy(gameObject);
    }

    public void ErruptionHit(Collider hit)
    {
        if (!isServer)
            return;

        var rec = hit.GetComponent<IReceiver>();
        if (rec.GetTeam() != _caster.GetTeam())
            rec.Damage(_caster.DamageMultiplier, DamagePerHit);
    }
}
