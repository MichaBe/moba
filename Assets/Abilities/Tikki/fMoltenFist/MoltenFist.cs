﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class MoltenFist : Spell, ICollisionHandler
{
    public Transform ActualObject;
    public float DurationComplete;
    public float DurationDot;
    public float DurationPunch;
    public float DurationTick;
    public float DamageInitial;
    public float DamageDot;
    public float MaxExtension;

    public override void AllUpdate()
    {
        Vector3 curPos = new Vector3(0, 1.5f, 0);
        curPos.z = Mathf.Min(1.0f, Mathf.InverseLerp(_networkStartTime, _networkStartTime+DurationPunch, (float)Network.time)) * MaxExtension;
        ActualObject.localPosition = curPos;
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + DurationComplete < Network.time)
            NetworkServer.Destroy(gameObject);
    }

    protected override void Execute()
    {
        transform.forward = _mousePositionAtCast - _casterPositionAtCast;
    }

    public void Collide(Collider hit)
    {
        if(isServer)
        {
            IReceiver rec = hit.GetComponent<IReceiver>();
            if(rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, DamageInitial);
                rec.DamageOverTime(_caster.DamageMultiplier, DamageDot, DurationDot, DurationTick);
            }
        }
    }
}
