﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AxeThrow : Spell {

    public float _range;
    public float _damage;
    public float _speed;

    public GameObject _actualAxe;
    [SyncVar]
    private bool _isFlying = false;
    private Vector3 _direction;
    private float _duration;

    
    protected override void Execute()
    {
        Debug.Log("Axtwurf gecasted");
        _direction = new Vector3(_mousePositionAtCast.x - _casterPositionAtCast.x, 0.0f, _mousePositionAtCast.z - _casterPositionAtCast.z).normalized;
        _duration = _range / _speed;
        _actualAxe.SetActive(true);
        _actualAxe.transform.localPosition = new Vector3(0.0f, 1.0f, 0.0f);
        _actualAxe.transform.forward = _direction;
        _isFlying = true;
    }
    

    public void ProjectileHit(IReceiver rec)
    {
        Debug.Log("ProjectileHit: isserver=" + isServer);
        if (isServer)
        {
            if (_caster.GetTeam() == rec.GetTeam())
                return;
            Debug.Log("ProjectileHit");
            rec.Damage(_caster.DamageMultiplier, _damage);
            NetworkServer.Destroy(gameObject);
        }
    }

    public override void AllUpdate()
    {
        if(_isFlying)
        {
            if(_networkStartTime + _duration > Network.time)
            {
                _actualAxe.transform.position = transform.position + (((float)Network.time - _networkStartTime) * _direction * _speed);
                _actualAxe.transform.position = new Vector3(_actualAxe.transform.position.x, 1.0f, _actualAxe.transform.position.z);
                _actualAxe.transform.RotateAround(_actualAxe.transform.position, _actualAxe.transform.right, 100 * Time.deltaTime * _speed);
            }
        }
    }

    public override void ServerUpdate()
    {
        if (_isFlying && _networkStartTime + _duration <= Network.time)
        {
            NetworkServer.Destroy(gameObject);
            //TODO funktioniert noch nciht richtig
        }
    }


}
