﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BjörnQCollider : MonoBehaviour {
    
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter");
        if (other.transform.GetComponent<IReceiver>() != null)
        {
            Debug.Log("OnTriggerEnter");
            transform.parent.GetComponent<AxeThrow>().ProjectileHit(other.transform.GetComponent<IReceiver>());
        }
    }
}
