﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Whirlwind : Spell {

    public float Damage;
    public float Radius;
    public float Duration;
    public float Tick;
    private float _tickTimer;
    private int _casterTeam;


    protected override void Execute()
    {
        Debug.Log("Wirbelwind gecasted");
        _tickTimer = _networkStartTime;

        if (isServer)
        {
            _casterTeam = _caster.GetTeam();
            _caster.GetComponent<CharacterLoader>().Slow(50.0f, Duration);
        }
    }

    public override void AllUpdate()
    {
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + Duration >= Network.time)
        {
            if (Network.time - _tickTimer >= Tick) //Übersprungene ticks aufholen
            {
                Collider[] colliders = Physics.OverlapSphere(_caster.transform.position, Radius);

                foreach (Collider c in colliders)
                {
                    var temp = c.GetComponent<IReceiver>();
                    if (temp == null)
                        continue;

                    Debug.Log("Got IReciever: " + temp.GetTeamAndMemberHash());

                    if (temp.GetTeam() != _casterTeam)
                        temp.Damage(_caster.DamageMultiplier, Damage);
                }
                _tickTimer += Tick;
            }
        }
        else
        {
            NetworkServer.Destroy(gameObject);
        }
    }
}
