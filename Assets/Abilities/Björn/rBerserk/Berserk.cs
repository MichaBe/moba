﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Berserk : Spell {

    public float DamagePercent;
    public float SpeedPercent;
    public float Duration;

    // Update is called once per frame
    protected override void Execute()
    {
        Debug.Log("Berserk gecasted");
        if (isServer)
        {
            _caster.GetComponent<CharacterLoader>().SpeedUp(SpeedPercent, Duration);
            _caster.GetComponent<CharacterLoader>().DamagePercent(DamagePercent, Duration);
        }
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + Duration <= Network.time)
            NetworkServer.Destroy(gameObject);
    }

    public override void AllUpdate()
    {
    }
}
