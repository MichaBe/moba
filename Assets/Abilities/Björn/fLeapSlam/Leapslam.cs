﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Leapslam : Spell
{
    public float Damage;
    public float Radius;
    public float Duration;
    public float JumpAngle;

    protected override void Execute()
    {
        Debug.Log("Leapslam gecasted");

        if (isServer)
            _caster.GetComponent<CharacterLoader>().JumpToPositionWithDuration(Duration, _mousePositionAtCast);
    }

    public override void AllUpdate()
    {
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + Duration <= Network.time)
        {
            Collider[] colliders = Physics.OverlapSphere(_mousePositionAtCast, Radius);
            foreach (var c in colliders)
            {
                var temp = c.GetComponent<IReceiver>();
                if (temp == null)
                    continue;

                if (temp.GetTeam() == _caster.GetTeam())
                    continue;

                temp.Damage(_caster.DamageMultiplier, Damage);
                temp.KnockBackFrom(50.0f, _mousePositionAtCast);
            }
            NetworkServer.Destroy(gameObject);
        }
    }
}
