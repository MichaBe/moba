﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AA_RangedRight : Spell, ICollisionHandler {

    private float _speed = 30.0f;
    private float _range = 20.0f;
    private float _damage = 70.0f;

    private float _duration;
    private Vector3 _direction;
    private Transform _child;

    public override void AllUpdate()
    {
        float lerp = Mathf.InverseLerp(_networkStartTime, _networkStartTime + _duration, (float)Network.time);
        Vector3 v3Pos = _casterPositionAtCast + _direction * _range * lerp;
        v3Pos.y = 1.0f;
        _child.position = v3Pos;
    }

    public void Collide(Collider hit)
    {
        if (isServer)
        {
            IReceiver rec = hit.GetComponent<IReceiver>();
            if (rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, _damage);
            }
        }
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + _duration <= Network.time)
        {
            NetworkServer.Destroy(gameObject);
        }
    }

    protected override void Execute()
    {
        _child = GetComponentInChildren<Transform>();
        _direction = (_mousePositionAtCast - _casterPositionAtCast).normalized;
        _child.forward = _direction;
        _duration = _range / _speed;
    }
}
