﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AA_MeleeRight : Spell {

    private float _damage = 70.0f;
    private float _duration = 0.25f;
    private float _range = 1.0f;

    private List<int> alreadyHit;

    private void OnTriggerEnter(Collider other)
    {
        IReceiver rec = other.GetComponent<IReceiver>();
        if (isServer && rec != null)
        {
            if (alreadyHit == null)
                alreadyHit = new List<int>();
            if (rec.GetTeam() != _caster.GetTeam() && !alreadyHit.Contains(rec.GetTeamAndMemberHash())) {
                rec.Damage(_caster.DamageMultiplier, _damage);
                alreadyHit.Add(rec.GetTeamAndMemberHash());
            }
        }
    }

    public override void AllUpdate()
    {
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + _duration < Network.time)
            NetworkServer.Destroy(gameObject);
    }

    protected override void Execute()
    {
        if (isServer)
        {
            if(alreadyHit == null)
                alreadyHit = new List<int>();
        }
    }
}
