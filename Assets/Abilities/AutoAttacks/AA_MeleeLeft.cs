﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AA_MeleeLeft : Spell, ICollisionHandler
{
    public Transform _actualObject;
    private float _durationComplete = 0.5f;
    private float _durationPunch = 0.1f;
    private float _damage = 50.0f;
    private float _maxExtension = 3.0f;

    public override void AllUpdate()
    {
        Vector3 curPos = new Vector3(0, 1.5f, 0);
        curPos.z = Mathf.Min(1.0f,Mathf.InverseLerp(_networkStartTime,_networkStartTime+_durationPunch,(float)Network.time))*_maxExtension;
        _actualObject.localPosition = curPos;
    }

    public override void ServerUpdate()
    {
        if (_networkStartTime + _durationComplete < Network.time)
            NetworkServer.Destroy(gameObject);
    }

    protected override void Execute()
    {
        transform.forward = _mousePositionAtCast - _casterPositionAtCast;
    }

    public void Collide(Collider hit)
    {
        if(isServer)
        {
            IReceiver rec = hit.GetComponent<IReceiver>();
            if(rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, _damage);
            }
        }
    }
}
