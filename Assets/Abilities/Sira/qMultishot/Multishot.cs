﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Multishot : Spell {

    public GameObject[] _arrows;

    public GameObject _empty;
    public float _emptyStart;

    [Header("Balancing")]
    public float _damage;
    public float _speed;
    public float _range;

    private float _endTime;
    private Vector3[] directions;

    
    protected override void Execute()
    {
        transform.right = (_mousePositionAtCast - _casterPositionAtCast).normalized;
        _empty.transform.right = (_mousePositionAtCast - _casterPositionAtCast).normalized;
        _endTime = _networkStartTime + _range / _speed;
        directions = new Vector3[4];
        for(int i = 1; i < 5; ++i)
        {
            directions[i - 1] = (_arrows[i].transform.localPosition - _arrows[0].transform.localPosition);
        }
    }

    public override void AllUpdate()
    {
        float lerpFactor = Mathf.InverseLerp(_networkStartTime, _endTime, (float)Network.time);
        _empty.transform.localPosition = new Vector3(_emptyStart + lerpFactor * _range, 1.22f, 0.0f);
        
        for(int i = 1; i < 5; ++i)
        {
            _arrows[i].transform.localPosition = _arrows[0].transform.localPosition+directions[i-1] * (lerpFactor*4.0f+ 1.0f);
        }
    }


    public override void ServerUpdate()
    {
        if (Network.time > _endTime)
            NetworkServer.Destroy(gameObject);
    }

    public void Collide(Collider hit, int index)
    {
        if(isServer)
        {
            IReceiver rec = hit.GetComponent<IReceiver>();
            if (rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, _damage);
                _arrows[index].SetActive(false);
                RpcDeactivateArrow(index);
            }
        }
    }
    
    [ClientRpc]
    private void RpcDeactivateArrow(int index)
    {
        _arrows[index].SetActive(false);
    }
}
