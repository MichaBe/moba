﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultishotCollider : MonoBehaviour {

    public int _index;

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IReceiver>() != null)
        {
            transform.parent.parent.GetComponent<Multishot>().Collide(other, _index);
        }
    }

}
