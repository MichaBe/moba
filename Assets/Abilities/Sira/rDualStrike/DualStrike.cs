﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DualStrike : Spell, ICollisionHandler {

    public float _angleJump1;
    public float _angleJump2;

    public GameObject _arrow;

    [Header("Balancing")]
    public float _distanceJump;
    public float _damageStrike1;
    public float _durationStrike1;
    public float _rangeStrike1;
    public float _rangeShot;
    public float _speedShot;
    public float _damageStrike2;

    private float _durationShot;
    private int _curPhase;
    private float _curPhaseStartTime;
    private float _durationCurrentPhase;
    private IReceiver _ownReceiver;

    public override void AllUpdate()
    {
        if(_curPhase == 2)//Animation des ersten strikes ausführen
        {
            //TODO
        }
        else if(_curPhase == 4)//Animation des schusses ausführen
        {
            float lerpFactor = Mathf.InverseLerp(_curPhaseStartTime, _curPhaseStartTime + _durationCurrentPhase, (float)Network.time);
            Vector3 newPosition = transform.position + (_mousePositionAtCast-transform.position).normalized * (lerpFactor*_rangeShot);
            newPosition.y = 1.0f;
            _arrow.transform.position = newPosition;
        }
    }

    public override void ServerUpdate()
    {
        if(_curPhase == 0)//Starte den ersten Sprung
        {
            _ownReceiver.JumpToPositionWithAngle(_angleJump1, _mousePositionAtCast);
            _curPhaseStartTime = (float)Network.time;
            _durationCurrentPhase = Position.GetDurationOfJump(_caster.transform.position, _mousePositionAtCast, _angleJump1);
            _curPhase++;
            RpcUpdatePhaseVariables(_curPhase, _curPhaseStartTime, _durationCurrentPhase);
        }
        else if(_curPhase == 1 && _curPhaseStartTime+_durationCurrentPhase <= Network.time)//Bei ankunft: verursache schaden
        {
            Collider[] colliders = Physics.OverlapSphere(_caster.transform.position, _rangeStrike1);
            foreach(Collider c in colliders)
            {
                IReceiver temp = c.GetComponent<IReceiver>();
                if(temp != null)
                {
                    if(temp.GetTeam() != _caster.GetTeam())
                    {
                        temp.Damage(_caster.DamageMultiplier, _damageStrike1);
                        break;
                    }
                }
            }
            _curPhaseStartTime = (float)Network.time;
            _durationCurrentPhase = _durationStrike1;
            _curPhase++;
            RpcUpdatePhaseVariables(_curPhase, _curPhaseStartTime, _durationCurrentPhase);
        }
        else if(_curPhase == 2 && _curPhaseStartTime+_durationCurrentPhase <= Network.time)//Bei ende des Strikes: springe zurück
        {
            _ownReceiver.JumpToPositionWithAngle(_angleJump2, transform.position);
            _curPhaseStartTime = (float)Network.time;
            _durationCurrentPhase = Position.GetDurationOfJump(_caster.transform.position, transform.position, _angleJump2);
            _curPhase++;
            RpcUpdatePhaseVariables(_curPhase, _curPhaseStartTime, _durationCurrentPhase);
        }
        else if(_curPhase == 3 && _curPhaseStartTime+_durationCurrentPhase <= Network.time)//bei ende des sprungs
        {
            StartStrike2();
            _curPhaseStartTime = (float)Network.time;
            _durationCurrentPhase = _durationShot;
            _curPhase++;
            RpcUpdatePhaseVariables(_curPhase, _curPhaseStartTime, _durationCurrentPhase);
        }
        else if(_curPhase == 4 && _curPhaseStartTime+_durationCurrentPhase<=Network.time)
        {
            NetworkServer.Destroy(gameObject);
        }
    }

    [ClientRpc]
    private void RpcUpdatePhaseVariables(int nextPhase, float startTime, float duration)
    {
        _curPhase = nextPhase;
        _curPhaseStartTime = startTime;
        _durationCurrentPhase = duration;
        if (_curPhase == 4)
            StartStrike2();
    }
    private void StartStrike2()
    {
        _arrow.SetActive(true);
        _arrow.transform.forward = (_mousePositionAtCast-_casterPositionAtCast).normalized;
    }

    protected override void Execute()
    {
        _durationShot = _rangeShot / _speedShot;
        _curPhase = 0;
        if (isServer)
            _ownReceiver = _caster.GetComponent<IReceiver>();
    }

    public void Collide(Collider hit)
    {
        if(isServer)
        {
            IReceiver rec = hit.GetComponent<IReceiver>();
            if(rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, _damageStrike2);
                NetworkServer.Destroy(gameObject);
            }
        }
    }
}
