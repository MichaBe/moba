﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blink : Spell
{
    [Header("Balancing")]
    public float _blinkDistance;
    public float _blinkSpeed;
    public float _mvSpeedBuffValue;
    public float _mvSpeedBuffDuration;


    public override void AllUpdate()
    {
    }

    public override void ServerUpdate()
    {
        if(_networkStartTime+_blinkDistance/_blinkSpeed <= Network.time)
        {
            _caster.RpcSetWeaponState(true, true);
            Destroy(gameObject);
        }
    }

    protected override void Execute()
    {
        if(isServer)
        {
            _caster.RpcSetWeaponState(false, false);
            Vector3 direction = _mousePositionAtCast - _casterPositionAtCast;
            direction.y = 0.0f;
            float distance = Mathf.Min((_mousePositionAtCast - _casterPositionAtCast).magnitude, _blinkDistance);
            IReceiver rec = _caster.GetComponent<IReceiver>();
            rec.Leap(_blinkSpeed, distance, direction);
            rec.Invisible(_blinkDistance / _blinkSpeed);
            rec.SpeedUp(_mvSpeedBuffValue, _mvSpeedBuffDuration);
        }
    }
}
