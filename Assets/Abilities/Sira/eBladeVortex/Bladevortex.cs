﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Bladevortex : Spell, ICollisionHandler
{
    public float _baseHeight;
    public float _varying;
    public GameObject[] _daggers;

    [Header("Balancing")]
    public float _transitionTime;
    public float _duration;
    public float _damagePerHit;
    public float _rotationspeed;

    private Vector3 _tempV3;
    private float _timeForOnetoAppear;
    private int _appeared;
    private bool _transitionIN;
    
    protected override void Execute()
    {
        _timeForOnetoAppear = _transitionTime / 16.0f;
        _appeared = 0;
        _transitionIN = true;
    }


    public override void AllUpdate()
    {
        if (_transitionIN) {
            if (_appeared < 16)
            {
                if(Network.time > _networkStartTime+_timeForOnetoAppear*_appeared)
                {
                    _daggers[_appeared].SetActive(true);
                    _appeared++;
                }
            }
            else
                _transitionIN = false;
        }
        if(!_transitionIN && Network.time > _networkStartTime+ _duration+_transitionTime+(16-_appeared)*_timeForOnetoAppear)
        {
            _appeared--;
            _daggers[(int)Mathf.Max(0, _appeared)].SetActive(false);
        }
        foreach(GameObject tr in _daggers)
        {
            _tempV3 = tr.transform.position;
            _tempV3.y = Vector3.Dot(Vector3.back,(transform.position-tr.transform.position).normalized)*_varying+_baseHeight;
            tr.transform.position = _tempV3;
        }
        transform.rotation = Quaternion.Euler(0,((float)Network.time-_networkStartTime)*_rotationspeed, 0);
    }

    public void Collide(Collider hit)
    {
        if(isServer)
        {
            IReceiver rec = hit.GetComponent<IReceiver>();
            if(rec.GetTeam() != _caster.GetTeam())
            {
                rec.Damage(_caster.DamageMultiplier, _damagePerHit);
            }
        }
    }

    public override void ServerUpdate()
    {
        if(Network.time > _networkStartTime+_duration+_transitionTime*2.0f)
        {
            NetworkServer.Destroy(gameObject);
        }
    }
}
