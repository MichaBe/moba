﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitySwapButton : MonoBehaviour {

    public Image _imgBorder;
    public Text _abilityName;
    public Image _abilityImage;

    private AbilitySwapList _parent;
    private Ability _ability;

    public void InitButton(AbilitySwapList parent, Ability ability, bool isActiveAtStart)
    {
        _parent = parent;
        _ability = ability;

        _abilityImage.sprite = _ability.Icon;
        _abilityName.text = _ability.name;

        _imgBorder.enabled = isActiveAtStart;
    }

	public void OnButtonSwitchAbilityClicked()
    {
        _imgBorder.enabled = _parent.SwitchToThisAbilityClicked(_ability.ID, this);
    }

    public void DeactivateBorder()
    {
        _imgBorder.enabled = false;
    }
}
