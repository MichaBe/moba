﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilitySwapList : MonoBehaviour {

    private AbilitySwapButton _currentActiveButton;
    private int _abilityType;

    private List<GameObject> _buttons;
    private AbilitySwapManager _manager;

    /// <summary>
    /// Initialisiert die Liste
    /// </summary>
    /// <param name="faehigkeit">0 = q-Fähigkeit, 1 = e-Fähigkeit, 2 = r-Fähigkeit</param>
    public void Init(AbilitySwapManager manager, GameObject buttonPrefab, AllCharacterCollection allCharacters, AllCharacterCollection.CharacterClass klasse, int faehigkeit, int choosenAbilityID)
    {
        if(_buttons != null)
        {
            foreach (GameObject go in _buttons)
                Destroy(go);
            _buttons.Clear();
            _currentActiveButton = null;
        }
        _manager = manager;
        _abilityType = faehigkeit;
        _buttons = new List<GameObject>();
        CharacterInfo[] ciArray = allCharacters._Fernkaempfer;
        if (klasse == AllCharacterCollection.CharacterClass.Nahkaempfer)
            ciArray = allCharacters._Nahkaempfer;
        else if (klasse == AllCharacterCollection.CharacterClass.Ranger)
            ciArray = allCharacters._Ranger;

        foreach (CharacterInfo ci in ciArray)
        {
            GameObject go = Instantiate(buttonPrefab, transform);
            go.GetComponent<AbilitySwapButton>().InitButton(this, ci.Abilities[faehigkeit], choosenAbilityID == ci.Abilities[faehigkeit].ID);
            if (choosenAbilityID == ci.Abilities[faehigkeit].ID)
                _currentActiveButton = go.GetComponent<AbilitySwapButton>();
            _buttons.Add(go);
        }
    }

    public bool SwitchToThisAbilityClicked(int abilityID, AbilitySwapButton btn)
    {

        bool didSwitch = _manager.OnButtonSwapAbilityClicked(abilityID, _abilityType);
        if(didSwitch)
        {
            _currentActiveButton.DeactivateBorder();
            _currentActiveButton = btn;
        }
        return didSwitch;
    }

}
