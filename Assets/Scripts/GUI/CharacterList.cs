﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterList : MonoBehaviour {

    public GameObject _listItemPrefab;
    public Lobby _lobby;
    private bool _canSelectCharacter;
    
    private AllCharacterCollection _allCharacters;
    private List<Button> _characterButtons;

    public void Init(AllCharacterCollection collection)
    {
        if(_characterButtons == null)
            _characterButtons = new List<Button>();
        foreach (Button btn in _characterButtons)
            Destroy(btn.gameObject);
        _characterButtons.Clear();
        _canSelectCharacter = true;

        _allCharacters = collection;
        //Fernkämpfer
        for(int i = 0; i < _allCharacters._Fernkaempfer.Length; ++i)
        {
            GameObject go = Instantiate(_listItemPrefab, transform);
            int iX = i;
            go.GetComponent<Button>().onClick.AddListener(delegate { OnCharacterClicked(_allCharacters._Fernkaempfer[iX].ID); });
            go.GetComponent<Image>().sprite = _allCharacters._Fernkaempfer[i].CharacterIcon;
            _characterButtons.Add(go.GetComponent<Button>());
        }
        //Ranger
        for (int i = 0; i < _allCharacters._Ranger.Length; ++i)
        {
            GameObject go = Instantiate(_listItemPrefab, transform);
            int iX = i;
            go.GetComponent<Button>().onClick.AddListener(delegate { OnCharacterClicked(_allCharacters._Ranger[iX].ID); });
            go.GetComponent<Image>().sprite = _allCharacters._Ranger[i].CharacterIcon;
            _characterButtons.Add(go.GetComponent<Button>());
        }
        //Nahkämpfer
        for (int i = 0; i < _allCharacters._Nahkaempfer.Length; ++i)
        {
            GameObject go = Instantiate(_listItemPrefab, transform);
            int iX = i;
            go.GetComponent<Button>().onClick.AddListener(delegate { OnCharacterClicked(_allCharacters._Nahkaempfer[iX].ID); });
            go.GetComponent<Image>().sprite = _allCharacters._Nahkaempfer[i].CharacterIcon;
            _characterButtons.Add(go.GetComponent<Button>());
        }
    }
	
    public void DidChooseCharacter()
    {
        _canSelectCharacter = false;
    }

    public void OnCharacterClicked(int CharacterID)
    {
        if(_canSelectCharacter)
            _lobby.ClientChooseCharacter(CharacterID);
    }

}
