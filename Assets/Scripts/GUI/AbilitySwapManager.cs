﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilitySwapManager : MonoBehaviour {

    public GameObject buttonPrefab;
    public AbilitySwapList[] _abilityButtonParents;
    public Text _AvailableSwaps;
    public Image _characterArtwork;

    private Lobby _lobby;

    private AllCharacterCollection _allCharacters;
    private AllCharacterCollection.CharacterClass _curClass;
    private CharacterInfo _curCharacter;
    private int[] _abilityIDsBeforeSwap;
    private int[] _abilityIDsAfterSwap;
    private int[] _tempIDsAfterSwap;

    
    public int[] GetAbilityIDsAfterSwap()
    {
        return _abilityIDsAfterSwap;
    }
    
    /// <returns>Anzahl zu tätigender Swaps in dieser Runde</returns>
    public int GetSwapCount(bool temp = false)
    {
        int iReturner = 0;
        if (temp)
        {
            for (int i = 0; i < _abilityIDsAfterSwap.Length; ++i)
            {
                if (_tempIDsAfterSwap[i] != _abilityIDsBeforeSwap[i])
                    iReturner++;
            }
        }
        else
        {
            for (int i = 0; i < _abilityIDsAfterSwap.Length; ++i)
            {
                if (_abilityIDsAfterSwap[i] != _abilityIDsBeforeSwap[i])
                    iReturner++;
            }
        }
        return iReturner;
    }

    /// <summary>
    /// Initialisiert die Abilitylisten. Muss vor jedem Abilityswap (also vor jeder Runde) neu aufgerufen werden
    /// </summary>
    public void Init(AllCharacterCollection allCharacters, int curCharacterID, int[] curAbilityIDs, Lobby localLobby, bool teamWon)
    {
        Debug.Log("AbilitySwapManager.Init");

        _lobby = localLobby;
        _abilityIDsBeforeSwap = new int[curAbilityIDs.Length];
        _abilityIDsAfterSwap = new int[curAbilityIDs.Length];
        _tempIDsAfterSwap = new int[curAbilityIDs.Length];
        _AvailableSwaps.text = _lobby._switchableAbilityCount.ToString();

        for(int i = 0; i < _abilityIDsAfterSwap.Length; ++i)
        {
            _abilityIDsAfterSwap[i] = _abilityIDsBeforeSwap[i] = _tempIDsAfterSwap[i] = curAbilityIDs[i];
        }
        _curCharacter = allCharacters.GetCharacterByID(curCharacterID);
        _allCharacters = allCharacters;
        _curClass = _allCharacters.GetClassByCharacterID(_curCharacter.ID);

        for(int i = 0; i < _abilityButtonParents.Length; ++i)
        {
            _abilityButtonParents[i].Init(this, buttonPrefab, _allCharacters, _curClass, i, curAbilityIDs[i]);
        }
        _characterArtwork.sprite = _curCharacter.CharacterArtWork;
    }

    public bool OnButtonSwapAbilityClicked(int AbilityID, int AbilityType)
    {
        _tempIDsAfterSwap[AbilityType] = AbilityID;
        bool swapPossible = (GetSwapCount(true) <= _lobby._switchableAbilityCount);

        if(swapPossible)
        {
            _abilityIDsAfterSwap[AbilityType] = AbilityID;
            _AvailableSwaps.text = (_lobby._switchableAbilityCount-GetSwapCount()).ToString();
        }
        else
        {
            _tempIDsAfterSwap[AbilityType] = _abilityIDsAfterSwap[AbilityType];
        }

        return swapPossible;
    }
}
