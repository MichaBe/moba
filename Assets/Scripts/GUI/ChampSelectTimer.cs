﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ChampSelectTimer : MonoBehaviour {

    private Text _text;
    private bool _didStart = false;
    private float _startTime;
    private float _countdownFromSec;

	// Use this for initialization
	void Start () {
        _text = GetComponent<Text>();
	}
	
    public void StartTimer(float NetworkStartTime, float CountdownFromSec)
    {
        _countdownFromSec = CountdownFromSec;
        _startTime = NetworkStartTime;
        _didStart = true;
    }

	// Update is called once per frame
	void Update () {
		if(_didStart)
        {
            _text.text = (Math.Max(_startTime + _countdownFromSec - Network.time, 0.0f)).ToString("F1");
        }
	}
}
