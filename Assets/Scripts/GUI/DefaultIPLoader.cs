﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DefaultIPLoader : MonoBehaviour {

    public InputField _input;

	// Use this for initialization
	void Start () {
	    if(File.Exists("IP.txt"))
        {
            string[] lines = File.ReadAllLines("IP.txt");
            _input.text = lines[0];
        }	
	}
}
