﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasOffAtStart : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Canvas>().enabled = false;
        Debug.Log("Turned " + gameObject.name + "-canvas off");
	}
}
