﻿using UnityEngine;
using UnityEngine.UI;

public class Login : MonoBehaviour {


    public InputField _inputUsername;
    public Text _nameField;
    
    public void OnLoginButtonClicked()
    {
        LocalDataContainer._localUsername = _inputUsername.text;
        _nameField.text = LocalDataContainer._localUsername;
        gameObject.SetActive(false);
    }
	
}
