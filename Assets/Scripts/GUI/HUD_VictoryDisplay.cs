﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD_VictoryDisplay : MonoBehaviour {

    public Image[] _teamImages;
    public Color[] _teamColors;
    public Sprite[] _winSprites;

	// Use this for initialization
	void Start () {
        for (int i = 0; i < _teamImages.Length; ++i)
        {
            _teamImages[i].color = _teamColors[i];
            _teamImages[i].sprite = _winSprites[0];
        }
    }

    public void SetTeamWins(int TeamNum, int wins)
    {
        _teamImages[TeamNum - 1].sprite = _winSprites[wins];
    }
}
