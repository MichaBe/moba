﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

    // Use this for initialization
    public Transform target;
    private Vector3 offset;

    void Start()
    {
        if (!target)
            Debug.LogError("No target found or target is not tagged!");
        offset = target.position - transform.position;
    }

    void Update()
    {
        transform.position = target.position - offset;
    }
}
