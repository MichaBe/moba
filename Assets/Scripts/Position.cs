﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Position
{
    public static Vector3 RandomCircumferencePoint(Vector3 position, float radius)
    {
        var vector2 = Random.insideUnitCircle.normalized * radius;
        return new Vector3(vector2.x, 0, vector2.y) + position;
    }

    public static float KnockBackFrom(GameObject gameObject, float force, Vector3 position)
    {
        Rigidbody rigid = gameObject.GetComponent<Rigidbody>();
        if (!rigid)
            return 0;

        rigid.velocity = Vector3.zero;

        Vector3 pushDirection = (gameObject.transform.position - position).normalized;
        Vector3 velocity = pushDirection * force * 100;
        rigid.AddForce(velocity);
        float duration = -velocity.y * 2 / Physics.gravity.y;
        return duration;
    }

    public static float JumpToPositionWithAngle(GameObject gameObject, float initialAngle, Vector3 position)
    {
        Rigidbody rigid = gameObject.GetComponent<Rigidbody>();
        if (!rigid)
            return 0;

        rigid.velocity = Vector3.zero;

        float gravity = Physics.gravity.magnitude;
        // Selected angle in radians
        float angle = initialAngle * Mathf.Deg2Rad;

        // Positions of this object and the target on the same plane_caster
        Vector3 planarTarget = new Vector3(position.x, 0, position.z);
        Vector3 planarPostion = new Vector3(gameObject.transform.position.x, 0, gameObject.transform.position.z);

        // Planar distance between objects
        float distance = Vector3.Distance(planarTarget, planarPostion);

        // Distance along the y axis between objects
        float yOffset = gameObject.transform.position.y - position.y;

        float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));

        Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));

        // Rotate our velocity to match the direction between the two objects
        float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPostion) * (position.x > gameObject.transform.position.x ? 1 : -1);
        Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;

        // Alternative way:
        // GetComponent<Rigidbody>().velocity = finalVelocity;
        rigid.AddForce(finalVelocity * rigid.mass, ForceMode.Impulse);

        float duration = -velocity.y * 2 / Physics.gravity.y;
        return duration;
    }

    public static void JumpToPositionWithDuration(GameObject gameObject, float duration, Vector3 position)
    {
        Rigidbody rigid = gameObject.GetComponent<Rigidbody>();
        if (!rigid)
            return;

        rigid.velocity = Vector3.zero;

        // For a given distance and jump duration
        // there is only one possible movement curve.
        // We are executing Y axis movement separately,
        // so we need to know a starting velocity.
        float velocityY = -duration * Physics.gravity.y / 2;

        // Positions of this object and the target on the same plane_caster
        Vector3 planarTarget = new Vector3(position.x, 0, position.z);
        Vector3 planarPostion = new Vector3(gameObject.transform.position.x, 0, gameObject.transform.position.z);

        // Planar distance between objects
        float distance = Vector3.Distance(planarTarget, planarPostion);
        float velocityX = distance / duration;

        Vector3 velocity = new Vector3(0, velocityY, velocityX);

        // Rotate our velocity to match the direction between the two objects
        float angleBetweenObjects = Vector3.Angle(Vector3.forward, planarTarget - planarPostion) * (position.x > gameObject.transform.position.x ? 1 : -1);
        Vector3 finalVelocity = Quaternion.AngleAxis(angleBetweenObjects, Vector3.up) * velocity;

        // Alternative way:
        // GetComponent<Rigidbody>().velocity = finalVelocity;
        rigid.AddForce(finalVelocity * rigid.mass, ForceMode.Impulse);
    }

    public static float GetDurationOfJump(Vector3 startPosition, Vector3 endPosition, float initialAngle)
    {
        float gravity = Physics.gravity.magnitude;
        // Selected angle in radians
        float angle = initialAngle * Mathf.Deg2Rad;

        // Positions of this object and the target on the same plane_caster
        Vector3 planarTarget = new Vector3(endPosition.x, 0, endPosition.z);
        Vector3 planarPostion = new Vector3(startPosition.x, 0, startPosition.z);

        // Planar distance between objects
        float distance = Vector3.Distance(planarTarget, planarPostion);
        // Distance along the y axis between objects
        float yOffset = startPosition.y - endPosition.y;

        float initialVelocity = (1 / Mathf.Cos(angle)) * Mathf.Sqrt((0.5f * gravity * Mathf.Pow(distance, 2)) / (distance * Mathf.Tan(angle) + yOffset));

        Vector3 velocity = new Vector3(0, initialVelocity * Mathf.Sin(angle), initialVelocity * Mathf.Cos(angle));

        float yInitialSpeed = velocity.y;
        //0 = v_i + a*t
        //-v_i = a*t
        //-v_i/a = t
        float time1 = yInitialSpeed / gravity;
        float distanceToFall = Mathf.Abs(yInitialSpeed * 0.5f * time1 - Mathf.Abs(yOffset));
        //d = 0.5*a*t*t
        //d/(0.5*a) = t*t
        //t = sqrt(2*d/a)
        float time2 = Mathf.Sqrt(2 * distanceToFall / gravity);

        return time1 + time2;
    }
}
