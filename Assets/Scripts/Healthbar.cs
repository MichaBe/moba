﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class Healthbar : NetworkBehaviour
{
    public Image healthbar;
    public PlayerHealth player;
    public Transform _HPBarAnchor;

    // Update is called once per frame
    void Update()
    {
        healthbar.fillAmount = player.Health / player.MaxHealth;
        transform.parent.position = Camera.main.WorldToScreenPoint(_HPBarAnchor.position);
    }

}
