﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class PlayerHealth : NetworkBehaviour
{
    public PostProcessingProfile ppp_dead;
    [SyncVar] public float Health;
    [SyncVar] public float MaxHealth;

    private Lobby _lobby;
    private CharacterLoader _charLoader;
    [HideInInspector]
    public GameObject _healthbar;

    private float saturationBefore;

    // Use this for initialization
    void Start ()
	{
        if (isServer)
        {
            _lobby = GameObject.Find("Lobby").GetComponent<Lobby>();
        }
        _charLoader = GetComponent<CharacterLoader>();
    }

    //Wird aufgerufen, sobald der Character im CharacterLoader gesetzt ist (ist in Start() noch nicht der Fall!)
    public void Init()
    {
        Debug.Log("PlayerHealth.Init");
        MaxHealth = GetComponent<CharacterLoader>()._currentCharacter.MaxHealth;
        Health = MaxHealth;
        if (isServer)
        {
            _lobby = GameObject.Find("Lobby").GetComponent<Lobby>();
        }
        _charLoader = GetComponent<CharacterLoader>();
        _healthbar.GetComponentInChildren<Healthbar>().healthbar.color = _charLoader.GetTeamColor();
    }


    public void TakeDamage(float amount)
    {
        if (!isServer || Health <= 0)
            return;

        Health -= amount;

        if (Health <= 0)
        {
            Health = 0;
            _lobby.ServerPlayerDied(_charLoader._teamNum, _charLoader._memberNum);
        }
    }

    public void TakeHeal(float amount)
    {
        if (!isServer || Health >= MaxHealth)
            return;

        if (amount >= (MaxHealth - Health))
        {
            Health = MaxHealth;
            return;
        }

        Health += amount;
    }

    [ClientRpc]
    public void RpcDied()
    {
        // hide player and disable movement

        if (isLocalPlayer)
        {
            GetComponent<Caster>()._canCast = false;
            Camera.main.GetComponent<PostProcessingBehaviour>().profile = ppp_dead;
        }
        GetComponent<Collider>().enabled = false;
        GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
        _charLoader._ActualCharacterModell.SetActive(false);
        _healthbar.GetComponent<Image>().enabled = false;
    }
}
