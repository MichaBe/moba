﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityCooldown : MonoBehaviour {

    public Image DarkMask;
    public Text CoolDownTextDisplay;
    public Ability Ability;

    [SerializeField]
    private Image _myButtonImage;
    private float _coolDownDuration;
    private float _coolDownTimeLeft;
    private float _nextReadyTime;

    void Start()
    {
        _myButtonImage = GetComponent<Image>();
    }

    public void Initialize(Ability selectedAbility)
    {
        Ability = selectedAbility;
        _myButtonImage.sprite = Ability.Icon;
        DarkMask.sprite = Ability.Icon;
        _coolDownDuration = Ability.CooldownSec;
        AbilityReady();
    }

    // Update is called once per frame
    void Update()
    {
        if (!Ability)
            return;

        bool coolDownComplete = Time.time > _nextReadyTime;
        if (coolDownComplete)
        {
            AbilityReady();
        }
        else
        {
            CoolDown();
        }
    }

    private void AbilityReady()
    {
        CoolDownTextDisplay.enabled = false;
        DarkMask.enabled = false;
    }

    private void CoolDown()
    {
        _coolDownTimeLeft -= Time.deltaTime;
        float roundedCd = Mathf.Round(_coolDownTimeLeft);
        CoolDownTextDisplay.text = roundedCd.ToString();
        DarkMask.fillAmount = (_coolDownTimeLeft / _coolDownDuration);
    }

    public void ButtonTriggered()
    {
        _nextReadyTime = _coolDownDuration + Time.time;
        _coolDownTimeLeft = _coolDownDuration;
        DarkMask.enabled = true;
        CoolDownTextDisplay.enabled = true;
    }
}
