﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllCharacterCollection : ScriptableObject {

    public enum CharacterClass { Fernkaempfer, Ranger, Nahkaempfer }

    public CharacterInfo[] _Fernkaempfer;
    public CharacterInfo[] _Ranger;
    public CharacterInfo[] _Nahkaempfer;

    private Dictionary<int,CharacterInfo> _characterByID = null;
    private Dictionary<int, Ability> _abilityByID = null;

    public bool CharacterIDexists(int CharacterID)
    {
        bool bReturner = false;
        for(int i = 0; i < _Fernkaempfer.Length && !bReturner; ++i)
        {
            if(_Fernkaempfer[i].ID == CharacterID)
                bReturner = true;
        }
        for(int i = 0; i < _Ranger.Length && !bReturner; ++i)
        {
            if (_Ranger[i].ID == CharacterID)
                bReturner = true;
        }
        for(int i = 0; i < _Nahkaempfer.Length && !bReturner; ++i)
        {
            if (_Nahkaempfer[i].ID == CharacterID)
                bReturner = true;
        }
        return bReturner;
    }

    public CharacterClass GetClassByCharacterID(int CharacterID)
    {
        CharacterClass returner = CharacterClass.Fernkaempfer;
        bool bFound = false;
        foreach(CharacterInfo ci in _Fernkaempfer)
        {
            if(ci.ID == CharacterID)
            {
                returner = CharacterClass.Fernkaempfer;
                bFound = true;
                break;
            }
        }
        if(!bFound)
        {
            foreach(CharacterInfo ci in _Ranger)
            {
                if(ci.ID == CharacterID)
                {
                    returner = CharacterClass.Ranger;
                    bFound = true;
                    break;
                }
            }
        }
        if(!bFound)
        {
            foreach(CharacterInfo ci in _Nahkaempfer)
            {
                if(ci.ID == CharacterID)
                {
                    returner = CharacterClass.Nahkaempfer;
                    bFound = true;
                    break;
                }
            }
        }

        return returner;
    }

    public CharacterInfo GetCharacterByID(int ID)
    {
        CharacterInfo returner = null;
        if(_characterByID == null)
        {
            _characterByID = new Dictionary<int, CharacterInfo>();
            foreach (CharacterInfo ci in _Fernkaempfer)
                _characterByID.Add(ci.ID, ci);
            foreach (CharacterInfo ci in _Ranger)
                _characterByID.Add(ci.ID, ci);
            foreach (CharacterInfo ci in _Nahkaempfer)
                _characterByID.Add(ci.ID, ci);
        }
        if(_characterByID.ContainsKey(ID))
        {
            returner = _characterByID[ID];
        }
        return returner;
    }

    public Ability GetAbilityByID(int ID)
    {
        if(_abilityByID == null)
        {
            _abilityByID = new Dictionary<int, Ability>();
            foreach(CharacterInfo ci in _Fernkaempfer)
            {
                foreach (Ability ab in ci.Abilities)
                    _abilityByID.Add(ab.ID, ab);
            }
            foreach(CharacterInfo ci in _Ranger)
            {
                foreach (Ability ab in ci.Abilities)
                    _abilityByID.Add(ab.ID, ab);
            }
            foreach(CharacterInfo ci in _Nahkaempfer)
            {
                foreach (Ability ab in ci.Abilities)
                    _abilityByID.Add(ab.ID, ab);
            }
        }
        Ability returner = null;
        if (_abilityByID.ContainsKey(ID))
            returner = _abilityByID[ID];

        return returner;
    }
}
