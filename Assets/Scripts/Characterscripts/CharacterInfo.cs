﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Game/Character")]
public class CharacterInfo : ScriptableObject {

    [Header("Informationen")]
    [Tooltip("Muss Unique sein!")]
    public int ID;
    public string Name;
    public string Description;
    public TextAsset characterStory;
    public Sprite CharacterIcon;
    public Sprite CharacterArtWork;
    [Header("Objekte, Positionierung & Kollision")]
    public GameObject ModelPrefab;
    public RuntimeAnimatorController Controller;
    public Avatar Avatar;
    public float HeightInUnits;
    public float RadiusInUnits;

    [Header("Balancing")]
    public float PhysicalDamage;
    public float Armor;
    public float MovementSpeed;
    public float MaxHealth;
    public float HealthRegeneration;

    public GameObject[] AutoAttackPrefabs;

    public Ability[] BasicAttacks;
    public Ability[] Abilities;
    
    public CharacterInfo()
    {
        BasicAttacks = new Ability[2];
        Abilities = new Ability[4];
    }
}
