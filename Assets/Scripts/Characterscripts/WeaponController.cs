﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour {

    [Tooltip("Waffe der fernkämpfer")]
    public GameObject RangedWeapon;
    [Tooltip("Waffe der melees")]
    public GameObject MeleeWeapon;

    public Transform PrimaryHand;
    public Transform SecondaryHand;

    public void SetNewState(bool bRanged, bool bMelee)
    {
        Debug.Log("SetNewState to " + bRanged + " " + bMelee);
        if(RangedWeapon != null)
            RangedWeapon.SetActive(bRanged);
        if(MeleeWeapon != null)
            MeleeWeapon.SetActive(bMelee);
    }

}
