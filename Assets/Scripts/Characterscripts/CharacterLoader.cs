﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CharacterLoader : NetworkBehaviour, IReceiver {

    [SyncVar]
    public int _characterID;

    [SyncVar]
    public int _teamNum;

    [SyncVar]
    public int _memberNum;

    public CharacterInfo _currentCharacter;
    public AllCharacterCollection _allCharacters;

    private int[] _curAbilityIDs;

    public Transform _HealthBarAnchor;
    public GameObject _HealthBarPrefab;

    public GameObject _ActualCharacterModell;
    private GameObject _ActualHealthbar;

    private PlayerHealth player;

    private float _MovementSpeed;
    private float _PhysicalDamage;
    private float _Armor;
    private float _HealthRegeneration;

    [SyncVar]
    public float Speedmultiplier;

    private bool _invulnerable;

    private void Init()
    {
        Debug.Log("characterLoader.Init");
        Debug.Log("CharacterID: " + _characterID + " TeamNum: " + _teamNum + "_membernum: " + _memberNum);
        _allCharacters = GameObject.Find("Lobby").GetComponent<LobbyGUIManager>()._allCharacters;
        _currentCharacter = _allCharacters.GetCharacterByID(_characterID);
        _HealthBarAnchor.localPosition = new Vector3(0.0f,_currentCharacter.HeightInUnits,0.0f);
        _ActualCharacterModell = Instantiate(_currentCharacter.ModelPrefab, transform);
        CapsuleCollider tempCC = GetComponent<CapsuleCollider>();
        tempCC.center = new Vector3(0.0f, _currentCharacter.HeightInUnits / 2.0f, 0.0f);
        tempCC.radius = _currentCharacter.RadiusInUnits;
        tempCC.height = _currentCharacter.HeightInUnits;
        GetComponent<Caster>().InitAbilities(_currentCharacter.BasicAttacks,_currentCharacter.Abilities, _currentCharacter);
        _curAbilityIDs = new int[3];
        for(int i = 0; i < 3; ++i)
        {
            _curAbilityIDs[i] = _currentCharacter.Abilities[i].ID;
        }
        Debug.Log("Characterloader.Init with " + _curAbilityIDs[0] + " " + _curAbilityIDs[1] + " " + _curAbilityIDs[2]);
        _MovementSpeed = _currentCharacter.MovementSpeed;
        _PhysicalDamage = _currentCharacter.PhysicalDamage;
        _Armor = _currentCharacter.Armor;
        GameObject healthbarParent = GameObject.Find("InGameSecondLayerCanvas");
        _ActualHealthbar = Instantiate(_HealthBarPrefab, healthbarParent.transform);
        Healthbar hpb = _ActualHealthbar.GetComponentInChildren<Healthbar>();
        hpb.player = GetComponent<PlayerHealth>();
        hpb._HPBarAnchor = _HealthBarAnchor;
        player = GetComponent<PlayerHealth>();

        player._healthbar = _ActualHealthbar;
        player.Init();

        _invulnerable = false;
        Speedmultiplier = 1.0f;

        GetComponent<Animator>().runtimeAnimatorController = _currentCharacter.Controller;
        GetComponent<Animator>().avatar = _currentCharacter.Avatar;
        GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        GetComponent<NetworkAnimator>().SetParameterAutoSend(1, true);
    }

    // Use this for initialization
    void Start()
    {
        Debug.Log("CharacterLoader.Start()");
        Init();
        transform.position = GameObject.Find("spawns").GetComponent<SpawnManager>().GetSpawn(_teamNum, _memberNum);
    }

    //Wird für jedes Playerobjekt ausgeführt, egal ob localplayer oder nicht.
    //Zum Ausführungszeitpunkt wird garantiert, dass alle Spielerobjekte existieren
    public void LateInit()
    {
        Debug.Log("LateInit()");
        GetComponent<PlayerMovement>()._canWalk = true;
        GetComponent<Caster>()._canCast = true;
        if (!isLocalPlayer)
            return;

        GameObject.Find("Lobby").GetComponent<Lobby>()._localGamePlayer = gameObject;
        //TODO alles initialisieren, was nur den lokalplayer angeht, z.b. GUI-Elemente für die Allies.
        
    }

    public void RoundResetBeforeCountdown()
    {
        SwapAbilities();
        if (isLocalPlayer)
        {
            transform.position = GameObject.Find("spawns").GetComponent<SpawnManager>().GetSpawn(_teamNum, _memberNum);
            CmdSetAbilityIDs(_curAbilityIDs[0], _curAbilityIDs[1], _curAbilityIDs[2]);
        }
    }

    public void RoundResetAfterCountdown()
    {
        GetComponent<Caster>()._canCast = true;
        GetComponent<PlayerMovement>()._canWalk = true;
    }

    public int[] GetAbilityIDs()
    {
        return _curAbilityIDs;
    }

    /// <summary>
    /// Wechselt die Fähigkeiten lokal und auf dem server aus
    /// </summary>
    /// <param name="abilityIDs">3-Feld lannges Array, das die neuen IDs für q,e und f enthält.</param>
    public void SetAbilityIDs(int[] abilityIDs)
    {
        for (int i = 0; i < 3; ++i)
            _curAbilityIDs[i] = abilityIDs[i];
    }

    [Command]
    private void CmdSetAbilityIDs(int newQ, int newE, int newF)
    {
        _curAbilityIDs[0] = newQ;
        _curAbilityIDs[1] = newE;
        _curAbilityIDs[2] = newF;
        
        SwapAbilities();
    }
    private void SwapAbilities()
    {
        GetComponent<Caster>().SwapSlots(_allCharacters.GetAbilityByID(_curAbilityIDs[0]),
            _allCharacters.GetAbilityByID(_curAbilityIDs[1]),
            _allCharacters.GetAbilityByID(_curAbilityIDs[2]));
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Müssen genau so auch im Caster für die Spells implementiert sein. Dadurch wird der Vergleich 
    //in den einzelnen Spells möglich, und z.b. "Friendly Fire", oder mehrfaches Treffen bei durchfliegenden
    //Geschossen wird ausgeschalten
    public int GetTeam()
    {
        return _teamNum;
    }

    public Color GetTeamColor()
    {
        switch (_teamNum)
        {
            case 1:
                return Color.red;
            case 2:
                return Color.green;
            case 3:
                return Color.blue;
            default:
                return Color.grey;
        }
    }

    public int GetTeamAndMemberHash()
    {
        return _teamNum * 100 + _memberNum;
    }

    public void Damage(float damageMultiplier, float amount)
    {
        if (_invulnerable)
            return;

        player.TakeDamage(amount * damageMultiplier);
    }

    public void Heal(float amount)
    {
        player.TakeHeal(amount);
    }

    public void DamageOverTime(float damageMultiplier, float amount, float durationSec, float tickDuration)
    {
        StartCoroutine(DamageOverTimeCoroutine(damageMultiplier, amount, durationSec, tickDuration));
    }

    public void HealOverTime(float amount, float durationSec, float tickDuration)
    {
        StartCoroutine(HealOverTimeCoroutine(amount, durationSec, tickDuration));
    }

    public void GiveArmorShield(float armorAmount, float durationSec)
    {
        throw new NotImplementedException();
    }

    public void KnockBackFrom(float force, Vector3 position)
    {
        RpcKnockBackFrom(force, position);
    }

    public void JumpToPositionWithAngle(float initialAngle, Vector3 position)
    {
        RpcJumpToPositionWithAngle(initialAngle, position);
    }

    public void JumpToPositionWithDuration(float duration, Vector3 position)
    {
        RpcJumpToPositionWithDuration(duration, position);
    }

    public void KnockUp(float durationSec)
    {
        throw new NotImplementedException();
    }

    public void RemoveAllDamageOverTime()
    {
        throw new NotImplementedException();
    }

    public void RemoveAllSlows()
    {
        throw new NotImplementedException();
    }

    public void DamagePercent(float percentage, float durationSec)
    {
        StartCoroutine(DamagePercentCoroutine(percentage, durationSec));
    }

    public void SetInvulnerable(float durationSec)
    {
        StartCoroutine(InvulnerableCoroutine(durationSec));
    }

    public void ShredArmor(float amount, float durationSec)
    {
        throw new NotImplementedException();
    }

    public void Slow(float percentage, float durationSec)
    {
        RpcSlow(percentage, durationSec);
    }

    public void SpeedUp(float percentage, float durationSec)
    {
        RpcSpeedUp(percentage, durationSec);
    }

    public void Root(float durationSec)
    {
        RpcRoot(durationSec);
    }

    public void Leap(float LeapSpeed, float LeapDistance, Vector3 direction)
    {
        RpcLeap(LeapSpeed, LeapDistance, direction);
    }

    public void Invisible(float duration)
    {
        RpcInvisible(duration);
    }

    [ClientRpc]
    private void RpcLeap(float LeapSpeed, float LeapDistance, Vector3 direction)
    {
        if (isLocalPlayer)
        {
            float leapduration = LeapDistance / LeapSpeed;
            Vector3 leapVector = direction.normalized * LeapSpeed;
            StartCoroutine(Leap(leapduration, leapVector));
        }
    }

    [ClientRpc]
    private void RpcInvisible(float duration)
    {
        StartCoroutine(CRInvisible(duration));
    }

    [ClientRpc]
    public void RpcKnockBackFrom(float force, Vector3 position)
    {
        float duration = Position.KnockBackFrom(gameObject, force, position);

        if (isLocalPlayer)
            StartCoroutine(JumpCoroutine(duration));
    }

    [ClientRpc]
    public void RpcJumpToPositionWithAngle(float initialAngle, Vector3 position)
    {
        float duration = Position.JumpToPositionWithAngle(gameObject, initialAngle, position);

        if (isLocalPlayer)
        {
            GetComponent<PlayerMovement>()._JumpPosition = position;
            StartCoroutine(JumpCoroutine(duration));
        }

        }

    [ClientRpc]
    public void RpcJumpToPositionWithDuration(float duration, Vector3 position)
    {
        Position.JumpToPositionWithDuration(gameObject, duration, position);

        if (isLocalPlayer)
        {
            GetComponent<PlayerMovement>()._JumpPosition = position;
            StartCoroutine(JumpCoroutine(duration));
        }
    }

    [ClientRpc]
    public void RpcSlow(float percentage, float durationSec)
    {
        if (isLocalPlayer)
            StartCoroutine(SlowCoroutine(percentage, durationSec));
    }

    [ClientRpc]
    public void RpcSpeedUp(float percentage, float durationSec)
    {
        if (isLocalPlayer)
            StartCoroutine(SpeedUpCoroutine(percentage, durationSec));
    }

    [ClientRpc]
    public void RpcRoot(float durationSec)
    {
        if (isLocalPlayer)
            StartCoroutine(RootCoroutine(durationSec));
    }

    private IEnumerator CRInvisible(float duration)
    {
        float endTime = Time.time + duration;
        SkinnedMeshRenderer renderer = GetComponentInChildren<SkinnedMeshRenderer>();
        while (Time.time < endTime)
        {
            renderer.enabled = false;
            yield return null;
        }
        renderer.enabled = true;
    }

    private IEnumerator Leap(float duration, Vector3 leapVector)
    {
        float starttime = Time.time;
        while (Time.time - starttime < duration)
        {
            transform.position += leapVector * Time.deltaTime;
            yield return null;
        }
    }

    public IEnumerator DamageOverTimeCoroutine(float damageMultiplier, float amount, float duration, float tickDuration)
    {
        float amountDamaged = 0;
        float damagePerLoop = (amount * tickDuration) / duration;
        while (amountDamaged < amount)
        {
            Damage(damageMultiplier, damagePerLoop);
            amountDamaged += damagePerLoop;
            yield return new WaitForSeconds(tickDuration);
        }
    }

    public IEnumerator HealOverTimeCoroutine(float amount, float durationSec, float tickDuration)
    {
        float amountHealed = 0;
        float healPerLoop = (tickDuration * amount) / durationSec;
        while (amountHealed < amount)
        {
            Heal(healPerLoop);
            amountHealed += healPerLoop;
            yield return new WaitForSeconds(tickDuration);
        }
    }

    private IEnumerator DamagePercentCoroutine(float percentage, float durationSec)
    {
        float amount = percentage / 100.0f;
        GetComponent<Caster>().DamageMultiplier += amount;
        yield return new WaitForSeconds(durationSec);
        GetComponent<Caster>().DamageMultiplier -= amount;
    }

    public IEnumerator InvulnerableCoroutine(float durationSec)
    {
        _invulnerable = true;
        yield return new WaitForSeconds(durationSec);
        _invulnerable = false;
    }

    public IEnumerator SlowCoroutine(float percentage, float durationSec)
    {
        float amount = percentage / 100.0f;
        Speedmultiplier -= amount;
        yield return new WaitForSeconds(durationSec);
        Speedmultiplier += amount;
    }

    public IEnumerator SpeedUpCoroutine(float percentage, float durationSec)
    {
        float amount = percentage / 100.0f;
        Speedmultiplier += amount;
        yield return new WaitForSeconds(durationSec);
        Speedmultiplier -= amount;
    }

    public IEnumerator RootCoroutine(float durationSec)
    {
        GetComponent<PlayerMovement>()._canWalk = false;
        GetComponent<PlayerMovement>()._canRotate = false;
        yield return new WaitForSeconds(durationSec);
        GetComponent<PlayerMovement>()._canWalk = true;
        GetComponent<PlayerMovement>()._canRotate = true;
    }

    private IEnumerator JumpCoroutine(float durationSec)
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<PlayerMovement>()._canWalk = false;
        GetComponent<PlayerMovement>()._canRotate = false;
        GetComponent<PlayerMovement>().IsJumping = true;
        yield return new WaitForSeconds(durationSec);
        GetComponent<Collider>().enabled = true;
        GetComponent<PlayerMovement>()._canWalk = true;
        GetComponent<PlayerMovement>()._canRotate = true;
        GetComponent<PlayerMovement>().IsJumping = false;
    }
}
