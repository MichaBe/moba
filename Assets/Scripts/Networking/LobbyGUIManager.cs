﻿using System;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class LobbyGUIManager : MonoBehaviour {
    
    [Header("UI MainMenu")]
    public GameObject _canvasMainMenu;
    public InputField _inputConnectToIP;
    public Dropdown _inputPlayersPerTeam;
    public Dropdown _inputTeamCount;

    [Header("UI TeamSelection")]
    public GameObject _canvasTeamSelection;
    public GameObject[] _emptyTeam1;
    public GameObject[] _emptyTeam2;
    public GameObject[] _emptyTeam3;
    public GameObject _btnLeave;
    public GameObject _btnStartChampSelect;
    public GameObject _emptySlotParent;
    public GameObject _emptySlotPrefab;

    [Header("UI ChampSelection")]
    public Image _teamNum;
    public Sprite _teamSprite1;
    public Sprite _teamSprite2;
    public Sprite _teamSprite3;
    public GameObject _canvasChampSelection;
    public GameObject _emptySelf;
    public GameObject _emptyAllied1;
    public GameObject _emptyAllied2;
    public ChampSelectTimer _timer;
    public AllCharacterCollection _allCharacters;
    public CharacterList _characterList;
    public Button _startRoundButton;

    [Header("UI Loadingscreen")]
    public GameObject _canvasLoadingScreen;

    [Header("UI Swap Abilities")]
    public GameObject _canvasSwapAbilities;
    public AbilitySwapManager _abilitySwapManager;
    public PostProcessingProfile _ppp_Alive;

    [Header("UI Endscreen")]
    public GameObject _canvasEndScreen;
    public Text _textEndScreen;
    public Button _btnContinue;

    private Lobby _lobby;
    private GameObject _canvasCurrentActive;
    private GameObject _canvasPrevious;
	
    public GameObject getEmptyForTeamselectByTeamAndMember(int teamNum, int memberNum)
    {
        if(teamNum == 1)
        {
            return _emptyTeam1[memberNum - 1];
        }
        else if(teamNum == 2)
        {
            return _emptyTeam2[memberNum - 1];
        }
        else
        {
            return _emptyTeam3[memberNum - 1];
        }
    }
    
	void Start () {
        OnResetGUI();
	}

    public void OnResetGUI()
    {
        Debug.Log("OnResetGUI");
        _lobby = gameObject.GetComponent<Lobby>();
        _btnContinue.interactable = false;
        _characterList.Init(_allCharacters);
        _canvasPrevious = null;
        _canvasCurrentActive = _canvasMainMenu;
        _canvasTeamSelection.SetActive(true);
        _canvasTeamSelection.GetComponent<Canvas>().enabled = false;
        _canvasChampSelection.SetActive(true);
        _canvasChampSelection.GetComponent<Canvas>().enabled = false;
        _canvasLoadingScreen.SetActive(true);
        _canvasLoadingScreen.GetComponent<Canvas>().enabled = false;
        _canvasSwapAbilities.SetActive(true);
        _canvasSwapAbilities.GetComponent<Canvas>().enabled = false;
        _canvasEndScreen.SetActive(true);
        _canvasEndScreen.GetComponent<Canvas>().enabled = false;
        _canvasMainMenu.SetActive(true);
        _canvasMainMenu.GetComponent<Canvas>().enabled = true;
    }

    private void ChangeToCanvas(GameObject nextActive)
    {
        _canvasCurrentActive.GetComponent<Canvas>().enabled = false;
        _canvasPrevious = _canvasCurrentActive;
        _canvasCurrentActive = nextActive;
        nextActive.GetComponent<Canvas>().enabled = true;
    }
    public void Back()
    {
        _canvasCurrentActive.GetComponent<Canvas>().enabled = false;
        _canvasPrevious.GetComponent<Canvas>().enabled = true;
        _canvasCurrentActive = _canvasPrevious;
    }

    public void OnButtonConnectToServerClicked()
    {
        Debug.Log("ConnectToServer UI");
        _lobby.networkAddress = _inputConnectToIP.text;
        _lobby.StartClient();
    }
    public void OnButtonHostAndPlayClicked()
    {
        Debug.Log("HostAndPlay UI");
        _lobby._playerCountPerTeam = Int32.Parse(_inputPlayersPerTeam.options[_inputPlayersPerTeam.value].text);
        _lobby._teamCount = Int32.Parse(_inputTeamCount.options[_inputTeamCount.value].text);
        _lobby.maxPlayers = _lobby._playerCountPerTeam*_lobby._teamCount;
        _lobby.minPlayers = _lobby.maxPlayers;
        _lobby.StopHost();
        _lobby.StartServer();
    }

    public void OnButtonQuitClicked()
    {
        Application.Quit();
    }

    public void OnButtonStartChampselectClicked()
    {
        _lobby.TryStartChampSelect();
    }

    public void SetGUIToServerMode()
    {
        didSwitchToLoadingScreen = false;
        _btnLeave.SetActive(false);
        _btnStartChampSelect.SetActive(true);
    }
    public void SetGUIToClientMode()
    {
        didSwitchToLoadingScreen = false;
        _btnLeave.SetActive(true);
        _btnStartChampSelect.SetActive(false);
    }

    public void SetTeamSelectEmptySlots(int TeamCount, int MemberCount)
    {
        //Zuerst eventuell schon bestehende icons löschen
        Debug.Log("SetTeamSelectEmptySlots with " + TeamCount + " teams and " + MemberCount + " members.");
        Image[] children = _emptySlotParent.GetComponentsInChildren<Image>();
        foreach(Image child in children)
        {
            Destroy(child.gameObject);
        }
        //für Team1
        for (int i = 0; i < MemberCount; ++i)
            PlaceNewEmptyslotAtPosition(_emptyTeam1[i].transform.position);
        //für Team2
        for (int i = 0; i < MemberCount; ++i)
            PlaceNewEmptyslotAtPosition(_emptyTeam2[i].transform.position);

        //Für Team3
        if(TeamCount > 2)
        {
            for (int i = 0; i < MemberCount; ++i)
                PlaceNewEmptyslotAtPosition(_emptyTeam3[i].transform.position);
        }
    }
    private void PlaceNewEmptyslotAtPosition(Vector3 v3Position)
    {
        GameObject go = Instantiate(_emptySlotPrefab, _emptySlotParent.transform);
        go.transform.position = v3Position;
    }

    public void OnButtonLeaveClicked()
    {
        _lobby.StopHost();
        Back();
    }
    public void FromMainMenuToTeamSelect()
    {
        ChangeToCanvas(_canvasTeamSelection);
    }

    public void FromTeamSelectToChampSelect(float fStartTime, float CountdownFromSec, int TeamID)
    {
        ChangeToCanvas(_canvasChampSelection);
        _timer.StartTimer(fStartTime, CountdownFromSec);
        if(TeamID == 1)
        {
            _teamNum.sprite = _teamSprite1;
            int iCurSlot = 0;
            for(int i = 0; i < 3; ++i)
            {
                if(_emptyTeam1[i].GetComponentInChildren<LobbyPlayer>() != null)
                {
                    if(iCurSlot == 0)
                    {
                        _emptyTeam1[i].GetComponentInChildren<LobbyPlayer>().TransitionToChampSelect(_emptyAllied1);
                        iCurSlot++;
                    }
                    else
                    {
                        _emptyTeam1[i].GetComponentInChildren<LobbyPlayer>().TransitionToChampSelect(_emptyAllied2);
                        break;
                    }
                }
            }
        }
        else if(TeamID == 2)
        {
            _teamNum.sprite = _teamSprite2;
            int iCurSlot = 0;
            for (int i = 0; i < 3; ++i)
            {
                
                if (_emptyTeam2[i].GetComponentInChildren<LobbyPlayer>() != null)
                {
                    if (iCurSlot == 0)
                    {
                        _emptyTeam2[i].GetComponentInChildren<LobbyPlayer>().TransitionToChampSelect(_emptyAllied1);
                        iCurSlot++;
                    }
                    else
                    {
                        _emptyTeam2[i].GetComponentInChildren<LobbyPlayer>().TransitionToChampSelect(_emptyAllied2);
                        break;
                    }
                }
            }
        }
        else
        {
            _teamNum.sprite = _teamSprite3;
            int iCurSlot = 0;
            for (int i = 0; i < 3; ++i)
            {
                if (_emptyTeam3[i].GetComponentInChildren<LobbyPlayer>() != null)
                {
                    if (iCurSlot == 0)
                    {
                        _emptyTeam3[i].GetComponentInChildren<LobbyPlayer>().TransitionToChampSelect(_emptyAllied1);
                        iCurSlot++;
                    }
                    else
                    {
                        _emptyTeam3[i].GetComponentInChildren<LobbyPlayer>().TransitionToChampSelect(_emptyAllied2);
                        break;
                    }
                }
            }
        }
    }

    public void OnChangeTeamClicked(int newTeam)
    {
        _lobby.ChangeTeam(newTeam);
    }

    public void DisableCharacterSelect()
    {
        _characterList.DidChooseCharacter();
    }

    private bool didSwitchToLoadingScreen = false;
    public void SwitchToLoadingScreen()
    {
        if (!didSwitchToLoadingScreen)
        {
            didSwitchToLoadingScreen = true;
            _canvasChampSelection.GetComponent<Canvas>().enabled = false;
            _canvasLoadingScreen.GetComponent<Canvas>().enabled = true;
        }
    }

    public void SwitchToInGame()
    {
        _canvasChampSelection.GetComponent<Canvas>().enabled = false;
        _canvasLoadingScreen.GetComponent<Canvas>().enabled = false;
    }

    public void ShowSwapAbilities(bool bWonRound)
    {
        Debug.Log("LobbyGuiManager.ShowSwapAbilities");
        Camera.main.GetComponent<PostProcessingBehaviour>().profile = _ppp_Alive;
        _canvasSwapAbilities.GetComponent<Canvas>().enabled = true;
        CharacterLoader curLoader = _lobby._localGamePlayer.GetComponent<CharacterLoader>();
        _abilitySwapManager.Init(_allCharacters, curLoader._characterID, curLoader.GetAbilityIDs(), _lobby, bWonRound);
    }

    public void OnButtonClientSwapAbilitiesFinishedClicked()
    {
        if(_abilitySwapManager.GetSwapCount() <= _lobby._switchableAbilityCount)
        {
            _lobby.ClientSwapAbilitiesFinished(_abilitySwapManager.GetSwapCount());
            _lobby._localGamePlayer.GetComponent<CharacterLoader>().SetAbilityIDs(_abilitySwapManager.GetAbilityIDsAfterSwap());
            HideSwapAbilities();
            //TODO evtl. text anzeigen, dass auf andere spieler gewartet wird?
        }
        else
        {
            //TODO fehlermeldung anzeigen: zuviele Fähigkeiten zum tauschen ausgewählt
        }
    }

    private void HideSwapAbilities()
    {
        _canvasSwapAbilities.GetComponent<Canvas>().enabled = false;
    }

    public void ShowEndScreen(bool didWin)
    {
        didSwitchToLoadingScreen = false;
        _canvasEndScreen.GetComponent<Canvas>().enabled = true;
        Camera.main.GetComponent<PostProcessingBehaviour>().profile = _ppp_Alive;
        if (didWin)
            _textEndScreen.text = "Victory";
        else
            _textEndScreen.text = "Defeat";
    }

    public void OnButtonContinueEndScreenClicked()
    {
        //TODO abmelden testen
        OnResetGUI();
        _lobby.OnClientContinueEndClicked();
    }
}
