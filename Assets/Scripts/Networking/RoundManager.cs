﻿using UnityEngine;
using System.Collections;

public class RoundManager
{
    public bool IsSpecialRound
    {
         get; private set;
    }
    public bool IsRoundFinished
    {
        get; private set;
    }

    private int _teamCount;
    private int _memberCount;
    public int _neededWinCount
    {
        get; private set;
    }

    private int _currentRoundWinningTeam;

    private int _currentRound;
    private bool[,] _playerAlive;
    private int[,] _playerDeathCount;
    private int[] _teamWinCount;

    public RoundManager(int teamCount, int memberCount, bool speedRoundPossible, int neededWins = 3)
    {
        _currentRoundWinningTeam = -1;
        _teamCount = teamCount;
        _memberCount = memberCount;
        _playerAlive = new bool[_teamCount, _memberCount];
        _playerDeathCount = new int[_teamCount, _memberCount];
        _teamWinCount = new int[_teamCount];
        IsSpecialRound = false;
        IsRoundFinished = false;
        for (int i = 0; i < _teamCount; ++i)
        {
            _teamWinCount[i] = 0;
            for (int j = 0; j < _memberCount; ++j)
            {
                _playerAlive[i, j] = true;
                _playerDeathCount[i, j] = 0;
            }
        }
        _neededWinCount = neededWins;
        _currentRound = 0;
    }

    /// <summary>
    /// Setzt alle Rundenspezifischen Variablen zurück
    /// </summary>
    /// <returns>Gibt zurück, ob es sich um eine Spezielle (schnelle) Runde handelt.</returns>
    public bool StartRound()
    {
        _currentRoundWinningTeam = -1;
        _currentRound++;
        IsRoundFinished = false;
        int teamsWithOneWinLeft = 0;
        for (int i = 0; i < _teamCount; ++i) {
            teamsWithOneWinLeft += _teamWinCount[i] == _neededWinCount - 1 ? 1 : 0;
            for (int j = 0; j < _memberCount; ++j)
                _playerAlive[i, j] = true;
        }
        IsSpecialRound = (teamsWithOneWinLeft == _teamCount - 1 && teamsWithOneWinLeft != 1);
        return IsSpecialRound;
    }

    /// <summary>
    /// Muss aufgerufen werden, wenn ein Spieler stirbt. Erhöht u.a. den Death-Counter
    /// </summary>
    /// <param name="teamNum">Teamnummer des Spielers (1-based)</param>
    /// <param name="memberNum">Membernummer des Spielers (1-based)</param>
    public void PlayerDied(int teamNum, int memberNum)
    {
        if (GetActiveTeamCount() > 1)
        {
            _playerAlive[teamNum - 1, memberNum - 1] = false;
            _playerDeathCount[teamNum - 1, memberNum - 1] += 1;

            if (GetActiveTeamCount() == 1)
            {
                IsRoundFinished = true;
                for (int i = 0; i < _teamCount; ++i)
                {
                    if (TeamCountPlayerAlive(i + 1) >= 1)
                    {
                        _currentRoundWinningTeam = i + 1;
                        _teamWinCount[i] += 1;
                    }
                }
            }
        }
    }

    public int GetRoundWinningTeam()
    {
        return _currentRoundWinningTeam;
    }

    public int GetActiveTeamCount()
    {
        int activeTeamCount = 0;
        for(int i = 0; i < _teamCount; ++i)
        {
            activeTeamCount += (TeamCountPlayerAlive(i + 1) != 0 ? 1 : 0);
        }
        if (IsSpecialRound)
            activeTeamCount -= (_teamCount - 2);
        return activeTeamCount;
    }

    /// <summary>
    /// Berechnet die Anzahl der Verbliebenen Spieler des Teams und gibt sie zurück
    /// </summary>
    /// <param name="teamNum">Teamnummer (1-based)</param>
    /// <returns>Anzahl verbliebener Spieler</returns>
    public int TeamCountPlayerAlive(int teamNum)
    {
        int playerAlive = 0;
        for (int i = 0; i < _memberCount; ++i)
            if (_playerAlive[teamNum - 1, i])
                playerAlive += 1;

        return playerAlive;
    }

    public int TeamWinCount(int teamNum)
    {
        return _teamWinCount[teamNum - 1];
    }
}
