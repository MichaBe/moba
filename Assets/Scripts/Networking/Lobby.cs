﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System;

public class Lobby : NetworkLobbyManager {

    public float _countDownTimeForChampselect;
    public bool _useCountdownTimerForChampselect;
    [Range(1,3)]
    public int _roundsToWinCount = 3;

    private LobbyGUIManager _guiManager;
    private LobbyPlayer[] _lobbyPlayers;
    private GameObject[] _gamePlayers;
    private Dictionary<NetworkConnection, int> _playerConnectionToTeamNum;
    private RoundManager _roundManager;

    [HideInInspector]
    public int _teamCount;
    [HideInInspector]
    public int _playerCountPerTeam;
    private int _curPlayerCount;

    private bool[] _slotInUse;
    private NetworkConnection[] _slotPlayerConn;
    private bool[] _didSwitchThisRound;
    private bool _closeAfterAllDisconnected;

    private int _connectioncountWithLoadedGameScene;
    //Lokale Variablen, die auf dem Client die entsprechenden werte haben.
    [HideInInspector]
    public LobbyPlayer _localPlayer = null;
    [HideInInspector]
    public GameObject _localGamePlayer = null;
    private NetworkConnection _connectionToServer = null;
    private bool _IsServer;
    [HideInInspector]
    public int _switchableAbilityCount;
    private int[] _localWinCounter;

	// Use this for initialization
	void Start () {
        _closeAfterAllDisconnected = false;
        Debug.Log("Lobby.start");
        Init();
        DontDestroyOnLoad(gameObject);
	}

    private void Init()
    {
        _localWinCounter = new int[3] { 0, 0, 0 };
        _switchableAbilityCount = 0;
        _IsServer = false;
        _guiManager = GetComponent<LobbyGUIManager>();
        _playerConnectionToTeamNum = new Dictionary<NetworkConnection, int>();
    }

    private void SlotIndexToTeamAndPlaceInTeam(int slotIndex, out int Team, out int PlaceInTeam)
    {
        Team = (slotIndex % _teamCount) + 1;
        PlaceInTeam = (slotIndex / _teamCount) + 1;
    }

    private int TeamAndPlaceInTeamToSlotIndex(int Team, int PlaceInTeam)
    {
        int iSlotIndex = (Team-1+(PlaceInTeam-1)*_teamCount);
        return iSlotIndex;
    }

    private bool FirstFreeSlotInTeam(int TeamID, out int FirstFreeSlot)
    {
        bool bReturner = false;
        FirstFreeSlot = 0;
        for(int i = 0; i < _playerCountPerTeam; ++i)
        {
            if(!_slotInUse[TeamAndPlaceInTeamToSlotIndex(TeamID,i+1)])
            {
                bReturner = true;
                FirstFreeSlot = TeamAndPlaceInTeamToSlotIndex(TeamID, i+1);
                break;
            }
        }
        return bReturner;
    }

    //Wird auf dem Client ausgeführt, wenn er disconnected / leaved
    public void OwnClientDisconnect()
    {
        _localPlayer = null;
        _connectionToServer = null;
        if (client.isConnected)
        {
            client.UnregisterHandler(MessageID.TransitionToChampSelect);
            client.UnregisterHandler(MessageID.AllPlayersReady);
            client.UnregisterHandler(MessageID.AllLoaded);
            client.UnregisterHandler(MessageID.GameFinished);
            client.UnregisterHandler(MessageID.RoundFinished);
            client.UnregisterHandler(MessageID.StartRound);
        }
    }

    public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
    {
        int iCurSlot = 0;
        while (_slotInUse[iCurSlot])
            ++iCurSlot;

        _slotInUse[iCurSlot] = true;
        _slotPlayerConn[iCurSlot] = conn;

        int iTeamNum = 0;
        int iPlaceInTeam = 0;
        SlotIndexToTeamAndPlaceInTeam(iCurSlot, out iTeamNum, out iPlaceInTeam);

        GameObject go = Instantiate(lobbyPlayerPrefab.gameObject,
            _guiManager.getEmptyForTeamselectByTeamAndMember(iTeamNum,iPlaceInTeam).transform);
        go.transform.localPosition = new Vector3(0, 0, 0);
        LobbyPlayer current = go.GetComponent<LobbyPlayer>();
        current._placeInTeamID = iPlaceInTeam;
        current._teamID = iTeamNum;
        current._characterID = (iPlaceInTeam%3)+1;
        current._memberCount = _playerCountPerTeam;
        current._teamCount = _teamCount;

        _lobbyPlayers[TeamAndPlaceInTeamToSlotIndex(iTeamNum,iPlaceInTeam)] = current;
        _playerConnectionToTeamNum.Add(conn, iTeamNum);
        _curPlayerCount++;
        return go;
    }
    
    //Wird auf dem Server ausgeführt, wenn die entsprechende Verbindung unterbrochen wird (per Leave oder per Disconnect)
    public override void OnLobbyServerDisconnect(NetworkConnection conn)
    {
        int iCurSlot = 0;
        for(;iCurSlot < _slotPlayerConn.Length; ++iCurSlot)
        {
            if(conn == _slotPlayerConn[iCurSlot])
            {
                break;
            }
        }
        _curPlayerCount--;
        _slotPlayerConn[iCurSlot] = null;
        _slotInUse[iCurSlot] = false;
        _playerConnectionToTeamNum.Remove(conn);

        if(_closeAfterAllDisconnected && _playerConnectionToTeamNum.Count == 0)
        {
            Debug.Log("Lobby.Shutdown()");
            _guiManager.OnResetGUI();
            Shutdown();
            Network.Disconnect();
            MasterServer.UnregisterHost();
        }
    }

    //Wird beim Start des Servers ausgeführt
    public override void OnLobbyStartServer()
    {
        Debug.Log("OnLobbyStartServer");
        _IsServer = true;
        _connectioncountWithLoadedGameScene = 0;
        _curPlayerCount = 0;
        _slotInUse = new bool[_teamCount * _playerCountPerTeam];
        _lobbyPlayers = new LobbyPlayer[_teamCount * _playerCountPerTeam];
        _gamePlayers = new GameObject[_teamCount * _playerCountPerTeam];
        _slotPlayerConn = new NetworkConnection[_teamCount * _playerCountPerTeam];
        _didSwitchThisRound = new bool[_teamCount * _playerCountPerTeam];
        for(int i = 0; i < _slotInUse.Length; ++i)
        {
            _slotInUse[i] = false;
            _slotPlayerConn[i] = null;
            _lobbyPlayers[i] = null;
            _didSwitchThisRound[i] = false;
        }
        _roundManager = new RoundManager(_teamCount, _playerCountPerTeam, false, _roundsToWinCount);
        NetworkServer.RegisterHandler(MessageID.ChangeToTeam1, ServerChangeTeamTo1);
        NetworkServer.RegisterHandler(MessageID.ChangeToTeam2, ServerChangeTeamTo2);
        NetworkServer.RegisterHandler(MessageID.ChangeToTeam3, ServerChangeTeamTo3);
        NetworkServer.RegisterHandler(MessageID.ChooseCharacter, ServerClientChooseCharacter);
        NetworkServer.RegisterHandler(MessageID.SwitchingFinished, ServerClientFinishedSwap);
        _guiManager.FromMainMenuToTeamSelect();
        _guiManager.SetGUIToServerMode();
    }
    //Wird aufgerufen, wenn der Verbindungsversuch gestartet wird.
    //2 sekunden geben, coroutine bricht versuch ab, wenn dieser dann nicht erfolgreich war
    public override void OnLobbyStartClient(NetworkClient lobbyClient)
    {
        Debug.Log("OnLobbyStartClient");
        Debug.Log(lobbyClient.ToString());
        StartCoroutine(WaitForSecondsBeforeTimeout(2.0f));
    }
    private IEnumerator WaitForSecondsBeforeTimeout(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Debug.Log("Connect nicht erfolgreich nach " + seconds + " Sekunden.");
        StopHost();
    }

    //Wird aufgerufen, wenn die verbindung erfolgreich aufgebaut werden konnte
    //Die coroutine, die die verbindung abbricht, wird hier gestoppt.
    public override void OnLobbyClientConnect(NetworkConnection conn)
    {
        _localWinCounter = new int[3] { 0, 0, 0 };
        Debug.Log("Connected called on the client");
        Debug.Log(conn.ToString());
        _connectionToServer = conn;
        client.RegisterHandler(MessageID.TransitionToChampSelect, ClientTransitionToChampSelect);
        client.RegisterHandler(MessageID.AllPlayersReady, ClientAllPlayersReady);
        client.RegisterHandler(MessageID.AllLoaded, ClientAllLoadedStartAtNetworkTime);
        client.RegisterHandler(MessageID.GameFinished, ClientGameFinished);
        client.RegisterHandler(MessageID.RoundFinished, ClientRoundFinished);
        client.RegisterHandler(MessageID.StartRound, ClientStartRound);
        StopAllCoroutines();
        _guiManager.FromMainMenuToTeamSelect();
        _guiManager.SetGUIToClientMode();
    }

    public void ChangeTeam(int newTeam)
    {
        if (newTeam == 1)
        {
            var msg = new IntegerMessage(_localPlayer._teamID * 10 + _localPlayer._placeInTeamID);
            _connectionToServer.Send(MessageID.ChangeToTeam1, msg);
        }
        if(newTeam == 2)
        {
            var msg = new IntegerMessage(_localPlayer._teamID * 10 + _localPlayer._placeInTeamID);
            _connectionToServer.Send(MessageID.ChangeToTeam2, msg);
        }
        if (newTeam == 3)
        {
            var msg = new IntegerMessage(_localPlayer._teamID * 10 + _localPlayer._placeInTeamID);
            _connectionToServer.Send(MessageID.ChangeToTeam3, msg);
        }
    }

    private void ServerChangeTeamTo1(NetworkMessage msg)
    {
        TryChangeToTeam(1, msg.conn);
    }
    private void ServerChangeTeamTo2(NetworkMessage msg)
    {
        TryChangeToTeam(2, msg.conn);
    }
    private void ServerChangeTeamTo3(NetworkMessage msg)
    {
        if(_teamCount == 3)
            TryChangeToTeam(3, msg.conn);
    }
    private void TryChangeToTeam(int newTeamNum, NetworkConnection conn)
    {
        Debug.Log("TryChangeToTeam " + newTeamNum);

        //Find old Slot
        int iOldSlot = 0;
        while (iOldSlot < _slotPlayerConn.Length && _slotPlayerConn[iOldSlot] != conn)
            iOldSlot++;

        //Find old Team & PlaceInTeam
        int iOldTeam, iOldPlaceInTeam;
        SlotIndexToTeamAndPlaceInTeam(iOldSlot, out iOldTeam, out iOldPlaceInTeam);

        //Dont work on Teamchanges to the same Team
        if (newTeamNum != iOldTeam)
        {
            int ifreeSlot = 100;
            bool gotSlot = FirstFreeSlotInTeam(newTeamNum, out ifreeSlot);
            //If team has free slot
            if(gotSlot)
            {
                int iNewTeam, iNewPlaceInTeam;
                SlotIndexToTeamAndPlaceInTeam(ifreeSlot, out iNewTeam, out iNewPlaceInTeam);
                //alten slot frei machen
                _slotInUse[iOldSlot] = false;
                _slotPlayerConn[iOldSlot] = null;
                //neuen slot belegen
                _slotInUse[ifreeSlot] = true;
                _slotPlayerConn[ifreeSlot] = conn;
                _lobbyPlayers[ifreeSlot] = _lobbyPlayers[iOldSlot];
                _lobbyPlayers[iOldSlot] = null;
                _playerConnectionToTeamNum[conn] = iNewTeam;
                //änderung allen mitteilen
                foreach(LobbyPlayer lp in _lobbyPlayers)
                {
                    if (lp != null)
                    {
                        if (lp._placeInTeamID == iOldPlaceInTeam && lp._teamID == iOldTeam)
                        {
                            lp._placeInTeamID = iNewPlaceInTeam;
                            lp._teamID = iNewTeam;
                            lp.UpdateViewAfterPlaceChange();
                            break;
                        }
                    }
                }
            }
        }
    }

    public void TryStartChampSelect()
    {
        _roundManager = new RoundManager(_teamCount, _playerCountPerTeam, false, _roundsToWinCount);
        if (_IsServer)
        {
            Debug.Log("TryStartChampSelect");
            bool allPlayerConnected = true;
            foreach (bool connected in _slotInUse)
                allPlayerConnected &= connected;

            if(allPlayerConnected)
            {
                Debug.Log("All Players are connected");
                MessageID.TransitionMessage msg = new MessageID.TransitionMessage();
                msg._startTime = (float)Network.time;
                StartCoroutine(WaitCountdownThenEnterGame(_countDownTimeForChampselect));
                foreach(NetworkConnection conn in  _slotPlayerConn)
                {
                    conn.Send(MessageID.TransitionToChampSelect, msg);
                }
                for(int i = 0; i < _lobbyPlayers.Length; ++i)
                {
                    _lobbyPlayers[i]._characterID = _lobbyPlayers[i]._placeInTeamID + 1;
                }
                Debug.Log("Time sent: " + msg._startTime);
            }
            else
            {
                //TODO fehlermeldung auf SErver anzeigen
            }
        }
    }

    private IEnumerator WaitCountdownThenEnterGame(float secToWait)
    {
        yield return new WaitForSeconds(secToWait);
        if (_useCountdownTimerForChampselect)
            ServerTransitionToGame();
    }

    private bool didSwitchToGameOnServer = false;
    private void ServerTransitionToGame()
    {
        Debug.Log("Lobby.ServerTransitionToGame");
        if(!didSwitchToGameOnServer)
        {
            didSwitchToGameOnServer = true;
            ServerChangeScene(playScene);
            EmptyMessage msg = new EmptyMessage();
            foreach(NetworkConnection conn in _slotPlayerConn)
            {
                conn.Send(MessageID.AllPlayersReady, msg);
            }
            _roundManager.StartRound();
        }
    }

    public override void OnLobbyServerPlayersReady()
    {
        Debug.Log("Recieved Ready messages.");
        ServerTransitionToGame();
    }

    private void ClientTransitionToChampSelect(NetworkMessage msg)
    {
        if (msg.conn == _connectionToServer)
        {
            MessageID.TransitionMessage transitionMessage = msg.ReadMessage<MessageID.TransitionMessage>();
            Debug.Log("Time received: " + transitionMessage._startTime);
            _localPlayer.TransitionToChampSelect(_guiManager._emptySelf);
            _guiManager.FromTeamSelectToChampSelect(transitionMessage._startTime, _countDownTimeForChampselect, _localPlayer._teamID);
        }
    }
    public void ClientChooseCharacter(int CharacterID)
    {
        if(!_IsServer && _localPlayer._characterID != CharacterID)
        {
            Debug.Log("ClientChooseCharacter with ID " + CharacterID);
            IntegerMessage intMSG = new IntegerMessage(CharacterID);
            _connectionToServer.Send(MessageID.ChooseCharacter, intMSG);
        }
    }

    public void ServerClientChooseCharacter(NetworkMessage msg)
    {
        IntegerMessage intMSG = msg.ReadMessage<IntegerMessage>();
        //Gibt es die characterid?
        if(_guiManager._allCharacters.CharacterIDexists(intMSG.value))
        {
            int iTeamNum = _playerConnectionToTeamNum[msg.conn];
            bool bCharacterFree = true;
            for(int i = 0; i < _playerCountPerTeam && bCharacterFree; ++i)
            {
                int iSlot = TeamAndPlaceInTeamToSlotIndex(iTeamNum, i+1);
                bCharacterFree &= (_lobbyPlayers[iSlot]._characterID != intMSG.value);
            }
            //Wenn der charakter noch frei ist:
            //Entsprechenden lobbyPlayer suchen und die characterID setzen
            if(bCharacterFree)
            {
                int iSenderSlotNum = 0;
                for(;iSenderSlotNum < _slotPlayerConn.Length; ++iSenderSlotNum)
                {
                    if (_slotPlayerConn[iSenderSlotNum] == msg.conn)
                        break;
                }
                _lobbyPlayers[iSenderSlotNum]._characterID = intMSG.value;
            }
        }
    }
    
    public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
    {
        Debug.Log("OnLobbyServerSceneLoadedForPlayer");
        LobbyPlayer current = lobbyPlayer.GetComponent<LobbyPlayer>();
        CharacterLoader charLoader = gamePlayer.GetComponent<CharacterLoader>();
        charLoader._characterID = current._characterID;
        charLoader._memberNum = current._placeInTeamID;
        charLoader._teamNum = current._teamID;
        _gamePlayers[TeamAndPlaceInTeamToSlotIndex(current._teamID, current._placeInTeamID)] = gamePlayer;
        _connectioncountWithLoadedGameScene++;
        if(_connectioncountWithLoadedGameScene == _teamCount*_playerCountPerTeam)
        {
            MessageID.TransitionMessage msg = new MessageID.TransitionMessage();
            msg._startTime = (float)Network.time + 2.0f;
            foreach(NetworkConnection conn in _slotPlayerConn)
            {
                conn.Send(MessageID.AllLoaded, msg);
            }
            //TODO selbst auch umschalten als server
        }
        return true;
    }

    private void ClientAllPlayersReady(NetworkMessage msg)
    {
        Debug.Log("ClientAllPlayersReady");
        _guiManager.SwitchToLoadingScreen();
    }

    private void ClientAllLoadedStartAtNetworkTime(NetworkMessage msg)
    {
        Debug.Log("Lobby.ClientAllLoadedStartAtNetworkTime()");
        MessageID.TransitionMessage tr_msg = msg.ReadMessage<MessageID.TransitionMessage>();
        StartCoroutine(WaitForGameStart(tr_msg._startTime - (float)Network.time));
    }
    private IEnumerator WaitForGameStart(float sec)
    {
        yield return new WaitForSeconds(sec);
        Debug.Log("After WaitForSeconds in WaitForGameStart() " + sec);
        _guiManager.SwitchToInGame();
        
        //LateInit für alle Objekte aufrufen
        CharacterLoader[] loaders = FindObjectsOfType<CharacterLoader>();
        foreach (CharacterLoader l in loaders)
            l.LateInit();
    }

    public void ServerPlayerDied(int TeamNum, int MemberNum)
    {
        Debug.Log("ServerPlayerDied with Team " + TeamNum + " and Membernum " + MemberNum);
        if (!_roundManager.IsRoundFinished)
        {
            _roundManager.PlayerDied(TeamNum, MemberNum);
            GameObject curGoPlayer = _gamePlayers[TeamAndPlaceInTeamToSlotIndex(TeamNum, MemberNum)];
            PlayerHealth curPlayer = curGoPlayer.GetComponent<PlayerHealth>();
            curGoPlayer.GetComponent<Collider>().enabled = false;
            curPlayer.RpcDied();
            if (_roundManager.IsRoundFinished)
            {
                for (int i = 0; i < _didSwitchThisRound.Length; ++i)
                    _didSwitchThisRound[i] = false;

                bool gameFinished = false;
                for (int i = 0; i < _teamCount && !gameFinished; ++i)
                    gameFinished = (_roundManager.TeamWinCount(i + 1) == _roundManager._neededWinCount);
                if(gameFinished)
                {
                    ServerGameFinished();
                }
                else
                {
                    ServerOnlyRoundFinished();
                }
            }
        }
        
    }

    private void ServerOnlyRoundFinished()
    {
        IntegerMessage roundFinishedMsg = new IntegerMessage(_roundManager.GetRoundWinningTeam());
        for(int i = 0; i < _slotPlayerConn.Length; ++i)
        {
            _slotPlayerConn[i].Send(MessageID.RoundFinished, roundFinishedMsg);
        }
    }

    private void ServerGameFinished()
    {
        IntegerMessage wonMsg = new IntegerMessage(1);
        IntegerMessage loseMsg = new IntegerMessage(0);
        for (int i = 0; i < _slotPlayerConn.Length; ++i)
        {
            int iTeam, iMember;
            SlotIndexToTeamAndPlaceInTeam(i, out iTeam, out iMember);
            if (_roundManager.TeamWinCount(iTeam) == _roundManager._neededWinCount)
            {
                _slotPlayerConn[i].Send(MessageID.GameFinished, wonMsg);
            }
            else
                _slotPlayerConn[i].Send(MessageID.GameFinished, loseMsg);
        }
        _closeAfterAllDisconnected = true;
        StartCoroutine(SwitchBackToLobbyAfterDelay());
    }

    private IEnumerator SwitchBackToLobbyAfterDelay()
    {
        yield return new WaitForSeconds(1.0f);//TODO testen
        ServerChangeScene(lobbyScene);
        _guiManager.OnResetGUI();
    }

    private void ClientRoundFinished(NetworkMessage msg)
    {
        _localGamePlayer.GetComponent<Caster>()._canCast = false;
        _localGamePlayer.GetComponent<PlayerMovement>()._canWalk = false;
        IntegerMessage int_msg = msg.ReadMessage<IntegerMessage>();//enthält die Teamnummer, die diese runde gewonnen hat
        _localWinCounter[int_msg.value - 1]++;

        //siegzähler setzen
        HUD_VictoryDisplay hud = GameObject.Find("HUD").GetComponent<HUD_VictoryDisplay>();
        for (int i = 0; i < _localWinCounter.Length; ++i)
            hud.SetTeamWins(i + 1, _localWinCounter[i]);

        bool bWonRound = false;
        if (int_msg.value == _localGamePlayer.GetComponent<CharacterLoader>()._teamNum)
        {
            _switchableAbilityCount += 2;
            bWonRound = true;
        }
        else
        {
            _switchableAbilityCount += 1;
        }

        //auf clients zur richtigen gui weiterleiten
        _guiManager.ShowSwapAbilities(bWonRound);
    }

    //Wird aufgerufen, wenn der Client die Abilities getauscht hat, und der Tausch gültig ist.
    //Gültigkeit wird bereits vor aufruf überprüft
    public void ClientSwapAbilitiesFinished(int swappedAbilityCount)
    {
        Debug.Log("Lobby.ClientSwapAbilitiesFinished");
        _switchableAbilityCount -= swappedAbilityCount;
        EmptyMessage msg = new EmptyMessage();
        _connectionToServer.Send(MessageID.SwitchingFinished, msg);
    }

    //Wird auf dem Server aufgerufen, wenn ein Client mit dem Switching fertig ist
    public void ServerClientFinishedSwap(NetworkMessage msg)
    {
        //überprüfen, ob alle geswapped haben
        int iCurSlot = 0;
        for(;iCurSlot < _slotPlayerConn.Length; ++iCurSlot)
        {
            if (_slotPlayerConn[iCurSlot] == msg.conn)
                break;
        }
        _didSwitchThisRound[iCurSlot] = true;
        bool allSwitched = true;
        for (int i = 0; i < _didSwitchThisRound.Length && allSwitched; ++i)
        {
            allSwitched &= _didSwitchThisRound[i];
        }

        //Wenn alle Geswapped haben: Nachricht mit Rundenstartzeit für nächste runde senden
        if(allSwitched)
        {
            _roundManager.StartRound();
            for(int i = 0; i < _gamePlayers.Length; ++i)
            {
                _gamePlayers[i].GetComponent<PlayerHealth>().Health = _gamePlayers[i].GetComponent<PlayerHealth>().MaxHealth;
                _gamePlayers[i].GetComponent<Collider>().enabled = true;
            }
            MessageID.TransitionMessage send_msg = new MessageID.TransitionMessage();
            send_msg._startTime = (float)Network.time + 2;
            for (int i = 0; i < _slotPlayerConn.Length; ++i)
                _slotPlayerConn[i].Send(MessageID.StartRound, send_msg);


        }
    }

    private void ClientStartRound(NetworkMessage msg)
    {
        Debug.Log("Lobby.ClientStartRound");
        MessageID.TransitionMessage trMSG = msg.ReadMessage<MessageID.TransitionMessage>();
        CharacterLoader[] cls = FindObjectsOfType<CharacterLoader>();
        foreach(CharacterLoader cl in cls)
        {
            cl.gameObject.GetComponent<Collider>().enabled = true;
            cl.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            cl.gameObject.GetComponent<CharacterLoader>()._ActualCharacterModell.SetActive(true);
            cl.gameObject.GetComponent<PlayerHealth>()._healthbar.GetComponent<Image>().enabled = true;
        }

        StartCoroutine(StartRoundAfterDelay(trMSG._startTime - ((float)Network.time)));
    }

    private IEnumerator StartRoundAfterDelay(float secDelay)
    {
        Debug.Log("Lobby.StartRoundAfter with " + secDelay);
        _localGamePlayer.GetComponent<CharacterLoader>().RoundResetBeforeCountdown();

        if(secDelay > 0.0f)
            yield return new WaitForSeconds(secDelay);

        _localGamePlayer.GetComponent<CharacterLoader>().RoundResetAfterCountdown();
    }

    private void ClientGameFinished(NetworkMessage msg)
    {
        IntegerMessage int_msg = msg.ReadMessage<IntegerMessage>();
        bool bWonGame = Convert.ToBoolean(int_msg.value);
        _guiManager.ShowEndScreen(bWonGame);
        _localGamePlayer.GetComponent<Caster>()._canCast = false;
        StartCoroutine(EnableContinueToEndButtonAfterDelay());
        //TODO testen
    }

    private IEnumerator EnableContinueToEndButtonAfterDelay()
    {
        yield return new WaitForSeconds(3);
        _guiManager._btnContinue.interactable = true;
    }

    public void OnClientContinueEndClicked()
    {
        Debug.Log("OnClientContinueEndClicked");
        _switchableAbilityCount = 0;
        _localPlayer = null;
        _localGamePlayer = null;
        _connectionToServer = null;

        StopHost();//TODO testen
    }

}