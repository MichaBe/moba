﻿using UnityEngine.Networking;

public static class MessageID
{
    public const short ChangeToTeam1 = MsgType.Highest + 1;
    public const short ChangeToTeam2 = MsgType.Highest + 2;
    public const short ChangeToTeam3 = MsgType.Highest + 3;

    public const short TransitionToChampSelect = MsgType.Highest + 4;
    public class TransitionMessage : MessageBase
    { 
        public float _startTime;
    }

    public const short ChooseCharacter = MsgType.Highest + 5;

    public const short AllPlayersReady = MsgType.Highest + 6;
    public const short AllLoaded = MsgType.Highest + 7;

    public const short RoundFinished = MsgType.Highest + 8;
    public const short SwitchingFinished = MsgType.Highest + 9;
    public const short StartRound = MsgType.Highest + 10;

    public const short GameFinished = MsgType.Highest + 11;
}
