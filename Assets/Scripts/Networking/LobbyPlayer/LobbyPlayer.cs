﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LobbyPlayer : NetworkLobbyPlayer {
    public LobbyPlayerGUIController _playerGuiController;

    [SyncVar]
    public int _teamCount = 0;

    [SyncVar]
    public int _memberCount = 0;

    [SyncVar(hook = "VarHookTeamID")]
    public int _teamID = 0;

    [SyncVar(hook = "VarHookPlaceID")]
    public int _placeInTeamID = 0;

    [SyncVar(hook = "VarHookCharacterID")]
    public int _characterID;

    private Lobby _lobby;
    private LobbyGUIManager _lobbyGUI;
    private static int _readyPlayerCount;
    private bool _isReady;

    private void VarHookTeamID(int newValue)
    {
        _teamID = newValue;
        UpdateViewAfterPlaceChange();
    }

    private void VarHookPlaceID(int newValue)
    {
        _placeInTeamID = newValue;
        UpdateViewAfterPlaceChange();
    }

    private void VarHookCharacterID(int newValue)
    {
        _characterID = newValue;
        Debug.Log("Neue CharacterID: " + _characterID);
        if(_lobbyGUI._allCharacters.CharacterIDexists(_characterID))
        {
            _playerGuiController.SetCharacterIcon(_lobbyGUI._allCharacters.GetCharacterByID(_characterID));
        }
    }

    public void UpdateViewAfterPlaceChange()
    {
        if (_teamID > 0 && _placeInTeamID > 0)
        {
            if (_lobbyGUI == null)
                _lobbyGUI = GameObject.Find("Lobby").GetComponent<LobbyGUIManager>();
            GameObject newParent = _lobbyGUI.getEmptyForTeamselectByTeamAndMember(_teamID, _placeInTeamID);
            transform.parent = newParent.transform;
            transform.localPosition = new Vector3(0, 0, 0);
        }
    }

    void Start () {
        Debug.Log("LobbyPlayer.Start");
        _lobby = GameObject.Find("Lobby").GetComponent<Lobby>();
        _lobbyGUI = GameObject.Find("Lobby").GetComponent<LobbyGUIManager>();
        UpdateViewAfterPlaceChange();
        VarHookCharacterID(_characterID);
        _readyPlayerCount = 0;
    }

    public override void OnStartLocalPlayer()
    {
        Debug.Log("LobbyPlayer.OnStartLocalPlayer");
        base.OnStartLocalPlayer();
        StartLocal();
    }

    private void StartLocal()
    {
        Debug.Log("LobbyPlayer.StartLocal");
        _playerGuiController.ChangeToLocalLayout();
        if (_lobby == null)
            _lobby = GameObject.Find("Lobby").GetComponent<Lobby>();
        if (_lobbyGUI == null)
            _lobbyGUI = GameObject.Find("Lobby").GetComponent<LobbyGUIManager>();

        _isReady = false;
        _lobby._localPlayer = this;
        _lobbyGUI._startRoundButton.onClick.AddListener(OnStartRoundClicked);
        _lobbyGUI.SetTeamSelectEmptySlots(_teamCount, _memberCount);
    }


    public void OnStartRoundClicked()
    {
        if (isLocalPlayer && !_isReady)
        {
            SendReadyToBeginMessage();
            _isReady = true;
        }
        _lobbyGUI.DisableCharacterSelect();
    }

    public override void OnClientEnterLobby()
    {
        base.OnClientEnterLobby();
        if (isLocalPlayer)
            StartLocal();
    }

    public void TransitionToChampSelect(GameObject newParent)
    {
        _playerGuiController.SwitchToChampSelectLayout();
        transform.parent = newParent.transform;
        transform.localPosition = new Vector3(0, 0, 0);
    }

    public override void OnClientReady(bool readyState)
    {
        base.OnClientReady(readyState);
        if (readyState)
            _readyPlayerCount++;
        else
            _readyPlayerCount--;

        //Wichtig: Der GUI-Wechsel wird nicht unbedingt vom Lobby-Objekt des Local-Players ausgelöst!! Wenn ein anderer Spieler später fertig ist, löst er den Wechsel aus.
        if (_readyPlayerCount == _teamCount*_memberCount)
        {
            _lobbyGUI.SwitchToLoadingScreen();//TODO muss auch ausgelöst werden, wenn der Countdown abgelaufen ist. Allerdings nicht, wenn der Countdown abgelaufen ist, und bereits gewechselt wurde!
        }
        Debug.Log("LobbyPlayer.OnClientReady with " + readyState);
        Debug.Log("Readyplayercount " + _readyPlayerCount);
    }

    public void OnDestroy()
    {
        Debug.Log("OnDestroy");
        if(GetComponent<PlayerHealth>() != null)
            Destroy(GetComponent<PlayerHealth>()._healthbar);
        if (isLocalPlayer)
        {
            _lobbyGUI._startRoundButton.onClick.RemoveAllListeners();
            _lobby.OwnClientDisconnect();
        }
    }
}
