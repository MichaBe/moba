﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyPlayerGUIController : MonoBehaviour {

    public GameObject _TeamSelectParent;
    public GameObject _ChampSelectParent;

    public Sprite _isSelf;
    public Sprite _isOther;
    public Image _playerSymbol;
    public Image _champSymbol;

	// Use this for initialization
	void Start () {
        _ChampSelectParent.SetActive(false);
        _TeamSelectParent.SetActive(true);
	}

    public void ChangeToLocalLayout()
    {
        Debug.Log("ChangeToLocalLayout");
        _playerSymbol.sprite = _isSelf;
    }
    
    public void SwitchToChampSelectLayout()
    {
        _TeamSelectParent.SetActive(false);
        _ChampSelectParent.SetActive(true);
    }

    public void SetCharacterIcon(CharacterInfo info)
    {
        _champSymbol.sprite = info.CharacterIcon;
    }
}
