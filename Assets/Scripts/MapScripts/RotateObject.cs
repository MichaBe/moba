﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour {

    public float rotSpeedX;
    public float rotSpeedY;
    public float rotSpeedZ;

    // Update is called once per frame
    void Update () {
        transform.Rotate(rotSpeedX * Time.deltaTime, rotSpeedY * Time.deltaTime, rotSpeedZ * Time.deltaTime);
	}
}
