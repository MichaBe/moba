﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public GameObject[] _spawnsTeam1;
    public GameObject[] _spawnsTeam2;
    public GameObject[] _spawnsTeam3;

    public Vector3 GetSpawn(int TeamNum, int MemberNum)
    {
        GameObject goReturner = null;
        if(TeamNum == 1)
        {
            goReturner = _spawnsTeam1[MemberNum - 1];
        }
        if(TeamNum == 2)
        {
            goReturner = _spawnsTeam2[MemberNum - 1];
        }
        if(TeamNum == 3)
        {
            goReturner = _spawnsTeam3[MemberNum - 1];
        }
        return goReturner.transform.position;
    }
}
