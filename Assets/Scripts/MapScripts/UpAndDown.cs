﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//:P

public class UpAndDown : MonoBehaviour {

    private float frequency = 2.0f;
    private float amplitude = 0.1f;

    private Vector3 startPosition;
    private Vector3 curPosition;
    private float y;
	// Use this for initialization
	void Start () {
        startPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        curPosition = startPosition;
        curPosition.y += Mathf.Sin(Time.time * frequency) * amplitude;
        transform.position = curPosition;
	}
}
