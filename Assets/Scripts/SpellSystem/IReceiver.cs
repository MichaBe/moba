﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IReceiver {
    int GetTeam();

    int GetTeamAndMemberHash();

    void Damage(float damageMultiplier, float amount);

    void RemoveAllDamageOverTime();

    void Heal(float amount);

    void HealOverTime(float amount, float durationSec, float tickDuration);

    void DamagePercent(float percentage, float durationSec);

    void Slow(float percentage, float durationSec);

    void SpeedUp(float percentage, float durationSec);

    void Root(float durationSec);

    void KnockUp(float durationSec);

    void KnockBackFrom(float force, Vector3 position);

    void JumpToPositionWithAngle(float initialAngle, Vector3 position);

    void JumpToPositionWithDuration(float duration, Vector3 position);

    void RemoveAllSlows();

    void GiveArmorShield(float armorAmount, float durationSec);

    void ShredArmor(float amount, float durationSec);

    void SetInvulnerable(float durationSec);

    void DamageOverTime(float damageMultiplier, float amount, float durationSec, float tickDuration);

    void Leap(float LeapSpeed, float LeapDistance, Vector3 direction);

    void Invisible(float duration);
}
