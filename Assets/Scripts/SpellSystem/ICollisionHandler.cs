﻿using UnityEngine;

public interface ICollisionHandler {

    void Collide(Collider hit);
}
