﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericColliderCallParentIfReceiver : MonoBehaviour {

    private void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<IReceiver>() != null)
        {
            GetComponentInParent<ICollisionHandler>().Collide(other);
        }
    }
}
