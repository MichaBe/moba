﻿using System;
using UnityEngine;
using UnityEngine.Networking;

[CreateAssetMenu (menuName ="Game/SpellSystem/Ability")]
[Serializable]
public class Ability : ScriptableObject
{
    public enum AbilityPrefabParent
    {
        None,
        Caster
    }

    public enum CastPreventionType
    {
        None,
        Stun,
        Silence,
        Root
    }

    public enum CastTargetType
    {
        Self,
        Front,
        Destination
    }

    [Header("Allgemeine Eigenschaften")]
    public Sprite Icon;
    public int ID;
    public string AbilityName;
    [Multiline]
    public string Description;
    [Multiline]
    public string ToolTip;
    [Space]
    public float CastTimeSec;
    public float CooldownSec;
    public string AnimationTrigger;
    public CastPreventionType PreventionType;
    public CastTargetType TargetType;
    public AbilityPrefabParent AbilityParent;
    public GameObject SpellPrefab;

    [Header("Target Type Front")]
    [Range(0, 100)]
    public float Width;

    [Header("Target Type Destination")]
    [Range(0, 100)]
    public float Radius;
    [Space]
    [Range(0, 100)]
    public float Range;

    public Ability()
    {
    }
}