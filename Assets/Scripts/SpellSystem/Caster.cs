﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class Caster : NetworkBehaviour
{
    private struct SlotInfo
    {
        public Ability ability;
        public string castButton;
        public float lastCastStartTime; //Cast startet sofort nach drücken des knopfes
        public float lastCooldownStartTime; //Cooldown startet nach dem ausführen der Fähigkeit (nach cast)
        
    }
    private CharacterInfo _currentCharacter;

    public bool _canCast;

    private SlotInfo[] _slots;
    [SyncVar]
    private bool _isCasting;
    [SyncVar]
    private int _currentActiveSlot;
    [SyncVar]
    private bool _interruptCastNextFrame;

    [SyncVar] public float DamageMultiplier;

    private PlayerMovement _playerMovement;
    private Vector3 _mousePositionWS;
    private List<AbilityCooldown> _abilityCooldowns;

    public GameObject Indicator;

    public Sprite AoeIndicator;
    public Sprite DirectionIndicator;

    // Use this for initialization
    private void Start ()
    {
	    _currentActiveSlot = 0;
        _isCasting = false;
        _interruptCastNextFrame = false;
        _playerMovement = GetComponent<PlayerMovement>();
        DamageMultiplier = 1.0f;

        if (!isLocalPlayer)
            return;

        _canCast = false; //Wird erst, wenn alle die szene geladen haben, auf true gesetzt.
        _abilityCooldowns = GameObject.Find("HUD").GetComponentInChildren<SkillCoolDown>().Cooldowns;
    }

    [ClientRpc]
    public void RpcSetWeaponState(bool bRanged, bool bMelee)
    {
        GetComponentInChildren<WeaponController>().SetNewState(bRanged, bMelee);
    }

    public int GetTeam()
    {
        return GetComponent<IReceiver>().GetTeam();
    }
    public int GetTeamAndMemberHash()
    {
        return GetComponent<IReceiver>().GetTeamAndMemberHash();
    }
    public void SwapSlots(Ability q, Ability e, Ability f)
    {
        Debug.Log("Caster.SwapSlots with " + q.ID + " " + e.ID + " " + f.ID);
        _slots[2].ability = q;
        _slots[3].ability = e;
        _slots[4].ability = f;
        for (int i = 0; i < 4; ++i)
        {
            _slots[i+2].lastCooldownStartTime = (float)Network.time - _slots[i+2].ability.CooldownSec - 1;
        }
        if(isLocalPlayer)
        {
            for(int i = 2; i < 6; ++i)//Icons aktualisieren
            {
                _abilityCooldowns[i].Initialize(_slots[i].ability);
            }
        }
    }

	/*
    public void InterruptCast()
    {
        if (_isCasting)
            _interruptCastNextFrame = true;
    }*/

    
    /// <summary>
    /// Gibt den Cooldown der entsprechenden Fähigkeit zurück.
    /// </summary>
    /// <param name="slot">0-3, zero-based index für die Fähigkeit</param>
    /// <returns>Cooldown in Sekunden</returns>
    public float GetAbilityCooldown(int slot)
    {
        return Math.Max(0.0f, _slots[slot + 2].lastCooldownStartTime +_slots[slot + 2].ability.CooldownSec - (float)Network.time);
    }
    /// <summary>
    /// Gibt den Cooldown des entsprechenden Basisangriffs zurück.
    /// </summary>
    /// <param name="slot">0-1, zero-based index für den Basisangriff</param>
    /// <returns>Cooldown in Sekunden</returns>
    public float GetBasicAttackCooldown(int slot)
    {
        return Math.Max(0.0f, _slots[slot].lastCooldownStartTime + _slots[slot].ability.CooldownSec - (float)Network.time);
    }

    

    //Wird von CharacterController aufgerufen
    //Initialisiert die Fähigkeiten und Basicattacks vor jeder Runde
    public void InitAbilities(Ability[] basicattacks,Ability[] abilities, CharacterInfo ci)
    {
        _currentCharacter = ci;
        _slots = new SlotInfo[6];
        for(int i = 0; i < 2; ++i)
        {
            if (basicattacks[i] != null)
            {
                _slots[i].ability = basicattacks[i];
                _slots[i].lastCooldownStartTime = (float)Network.time - _slots[i].ability.CooldownSec - 1;

                if (isLocalPlayer)
                    _abilityCooldowns[i].Initialize(basicattacks[i]);
            }
            _slots[i].castButton = "mouse " + i;
        }

        for (int i = 0; i < 4; ++i)
        {
            if (abilities[i] != null)
            {
                _slots[i+2].ability = abilities[i];
                _slots[i+2].lastCooldownStartTime = (float)Network.time - _slots[i+2].ability.CooldownSec - 1;

                if (isLocalPlayer)
                    _abilityCooldowns[i+2].Initialize(abilities[i]);
            }
            _slots[i+2].castButton = "Ability " + (i+1);
        }
    }

	// Update is called once per frame
    private void Update ()
    {
        if(isServer)
        {
            ServerUpdate();
        }
        if (isLocalPlayer)
        {
            PlayerUpdate();
        }
    }

    //Wird nur auf dem Server ausgeführt. Behandelt dinge wie
    //Castunterbrechung und Castende
    private void ServerUpdate()
    {
        if(_isCasting)
        {
            if(_interruptCastNextFrame)
            {
                //TODO interrupt ausführen
            }
            if (_slots[_currentActiveSlot].lastCastStartTime <= Network.time-_slots[_currentActiveSlot].ability.CastTimeSec)
            {
                int slot = _currentActiveSlot;
                float t = (float)Network.time;
                Vector3 v3Caster = transform.position;
                Vector3 v3Mouse = _mousePositionWS;
                _isCasting = false;
                _slots[slot].lastCooldownStartTime = t;
                StartAbility(this, _slots[slot].ability, v3Mouse, v3Caster, t, slot < 2, slot);
            }
        }
    }

    //Wird nur bei dem Spieler ausgeführt, der die Kontrolle über das Objekt hat
    //Hier wird die Inputbehandlung unternommen
    private void PlayerUpdate()
    {
        if (_isCasting)
        {
            if (_slots[_currentActiveSlot].ability.TargetType == Ability.CastTargetType.Destination)
                CmdSyncMousePos(_playerMovement.AoeWorldPosition);
            else
                CmdSyncMousePos(_playerMovement.MouseWorldPosition);
        }
        else if(_canCast)
        {
            for (int i = 0; i < 6; ++i)
            {
                if (CrossPlatformInputManager.GetButtonDown(_slots[i].castButton)
                    && _slots[i].lastCooldownStartTime <= Network.time - _slots[i].ability.CooldownSec
                    /*TODO prevention überprüfen (je nach fähigkeit)*/)
                {
                    //TODO bei point&click: Raycast auf entsprechenden layer (enemie / allie) & Raycasthit überprüfen
                    
                    if (true)
                    {
                        Debug.Log("StartCast" + i + ": Playerupdate " + _slots[i].ability.AbilityName);
                        if (_slots[i].ability.TargetType == Ability.CastTargetType.Destination)
                            CmdStartCast(i, _playerMovement.MouseWorldPosition);
                        else
                            CmdStartCast(i, _playerMovement.MouseWorldPosition);
                        break;
                    }
                }
            }
        }
    }

    //Wird ständig bei der castzeit vom Spieler aufgerufen und auf dem Server ausgeführt.
    //Beim erfolgreichen beenden der Castzeit wird die aktuellste Mausposition genommen und für die Fähigkeit verwendet
    [Command]
    private void CmdSyncMousePos(Vector3 curMousePositionWS)
    {
        _mousePositionWS = curMousePositionWS;
    }


    //Wird beim knopfdruck und erfolgreichen überprüfen vom Spieler auf dem Server aufgerufen
    //Überprüft selbst ncohmals cooldown und Prevention, und startet den Cast / die Fähigkeit bei Castzeit=0
    [Command]
    private void CmdStartCast(int slot, Vector3 mousePosition)
    {
        Debug.Log("CmdStartCast " + slot + " " + _slots[slot].ability.AbilityName+": Start");
        if(!_isCasting 
            &&_slots[slot].lastCooldownStartTime <= Network.time-_slots[slot].ability.CooldownSec
            /*TODO prevention auf dem Server überprüfen*/)
        {
            if(_slots[slot].ability.CastTimeSec != 0.0f)
            {
                Debug.Log("CmdStartCast: Starting Cast at " + Network.time + " with CastTime " + _slots[slot].ability.CastTimeSec);
                _isCasting = true;
                _currentActiveSlot = slot;
                _slots[slot].lastCastStartTime = (float)Network.time;
                _mousePositionWS = mousePosition;
                RpcStartCast(slot, (float)Network.time);
                RpcStartCastAnimation(slot);
            }
            else
            {
                //Fähigkeit sofort ausführen
                Debug.Log("CmdStartCast: Executing Ability with 0 casttime");
                Vector3 mp = mousePosition;
                if (_slots[slot].ability.TargetType == Ability.CastTargetType.Destination) {
                    float distanceToSpawnPosition = (mousePosition - transform.position).magnitude;
                    Vector3 direction = new Vector3(mousePosition.x - transform.position.x, 0.0f, mousePosition.z - transform.position.z).normalized;
                    mp = transform.position + direction * Mathf.Min(distanceToSpawnPosition, _slots[slot].ability.Range);
                }

                _currentActiveSlot = slot;
                _slots[_currentActiveSlot].lastCooldownStartTime = (float)Network.time;
                StartAbility(this, _slots[_currentActiveSlot].ability, mp, transform.position, (float)Network.time, slot < 2, slot);
                RpcStartCastAnimation (slot);
            }
        }
        else
        {
            Debug.Log("CmdStartCast: Check for slot " + slot + " failed");
        }
    }

    //Wird auf allen Clients ausgeführt. Der Server ruft diese Funktion auf, wenn die Castzeit gestartet wird
    [ClientRpc]
    private void RpcStartCast(int slot, float startTime)
    {
        _playerMovement.StartCast(_slots[slot].ability.TargetType == Ability.CastTargetType.Destination, _slots[slot].ability.Range);
        Debug.Log("RpcStartCast mit " +slot+"@"+startTime+": " + _slots[slot].ability.AbilityName);
        _currentActiveSlot = slot;
        _isCasting = true;
        _slots[slot].lastCastStartTime = startTime;
        DisplayIndicator(slot);
    }

    [ClientRpc]
    public void RpcStartCastAnimation(int slot)
    {
        GetComponent<NetworkAnimator>().SetTrigger(_slots[slot].ability.AnimationTrigger);
    }

    //Wird auf allen Clients ausgeführt. Der Server ruft diese Funktion auf, wenn der Cast unterbrochen wurde oder nach ablaufen der Castzeit beendet wurde
    [ClientRpc]
    private void RpcBreakCast(int slot, float breakTime)
    {
        Debug.Log("RpcBreakCast mit " + slot + "@" + breakTime);
        _playerMovement.StopCast();
        _isCasting = false;
        _slots[slot].lastCooldownStartTime = breakTime;
        if (isLocalPlayer)
        {
            _abilityCooldowns[slot].ButtonTriggered(); //TODO testen
            Indicator.GetComponent<SpriteRenderer>().sprite = null;
            Indicator.GetComponent<SpriteRenderer>().size = new Vector2(1.0f, 1.0f);
        }
    }

    //Startet die Fähigkeit. Wird auf dem Server ausgeführt. Hier wird das entsprechende Gameobject gespawned
    private void StartAbility(Caster caster, Ability ability, Vector3 mouseWorldPosition, Vector3 casterPosition, float time, bool isAutoAttack, int iSlot)
    {
        if (isAutoAttack)
        {
            Debug.Log("StartAbility für " + ability.AbilityName + "@" + time);
            _isCasting = false;
            RpcBreakCast(_currentActiveSlot, time);
            GameObject go = Instantiate(_currentCharacter.AutoAttackPrefabs[iSlot]);
            go.transform.position = casterPosition;
            NetworkServer.Spawn(go);
            Spell curSpell = go.GetComponent<Spell>();
            curSpell.InitializeAbility(this, ability, mouseWorldPosition, casterPosition, time);
            curSpell.ExecuteAbility();
        }
        else
        {
            Debug.Log("StartAbility für " + ability.AbilityName + "@" + time);
            _isCasting = false;
            RpcBreakCast(_currentActiveSlot, time);
            if (ability.AbilityParent == Ability.AbilityPrefabParent.Caster)
            {
                GameObject go = Instantiate(ability.SpellPrefab, transform);
                go.transform.localPosition = new Vector3(0, 0, 0);
                NetworkServer.Spawn(go);
                Spell curSpell = go.GetComponent<Spell>();
                curSpell.InitializeAbility(this, ability, mouseWorldPosition, casterPosition, time);
                curSpell.ExecuteAbility();
            }
            else
            {
                GameObject go = Instantiate(ability.SpellPrefab);
                go.transform.position = casterPosition;
                NetworkServer.Spawn(go);
                Spell curSpell = go.GetComponent<Spell>();
                curSpell.InitializeAbility(this, ability, mouseWorldPosition, casterPosition, time);
                curSpell.ExecuteAbility();
            }
        }
    }

    private void DisplayIndicator(int slot)
    {
        if (!isLocalPlayer)
            return;

        switch (_slots[slot].ability.TargetType)
        {
            case Ability.CastTargetType.Front:
                Indicator.GetComponent<SpriteRenderer>().sprite = DirectionIndicator;
                Indicator.GetComponent<SpriteRenderer>().size = new Vector2(_slots[slot].ability.Width, _slots[slot].ability.Range);
                break;
            case Ability.CastTargetType.Destination:
                Indicator.GetComponent<SpriteRenderer>().sprite = AoeIndicator;
                Indicator.GetComponent<SpriteRenderer>().size = new Vector2(_slots[slot].ability.Radius * 2, _slots[slot].ability.Radius * 2);
                break;
            default:
                break;
        }
    }
}
