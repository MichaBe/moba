﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Networking;

public abstract class Spell : NetworkBehaviour
{
    protected Caster _caster;
    protected Ability _ability;
    protected Vector3 _casterPositionAtCast;
    protected Vector3 _mousePositionAtCast;
    protected float _networkStartTime;

    //Initialisiert die Fähigkeit auf Server und allen Clients.
    //Wird vom Caster aufgerufen
    //VORSICHT: _caster und _ability haben nur auf dem Server gültige Werte.
    public void InitializeAbility(Caster caster, Ability ability, Vector3 mouseWorldPosition, Vector3 casterPosition, float networkStartTime)
    {
        _caster = caster;
        _ability = ability;
        RpcInitialize(mouseWorldPosition, casterPosition, networkStartTime);
        Initialize(mouseWorldPosition, casterPosition, networkStartTime);
    }

    //Initialisierung auf den Clients (nur die entsprechend benötigten werte)
    [ClientRpc]
    private void RpcInitialize(Vector3 mouseWorldPosition, Vector3 casterPosition, float networkStartTime)
    {
        Debug.Log("RpcInitialize");
        Initialize(mouseWorldPosition, casterPosition, networkStartTime);
    }

    private void Initialize(Vector3 mouseWorldPosition, Vector3 casterPosition, float networkStartTime)
    {
        _mousePositionAtCast = mouseWorldPosition;
        _casterPositionAtCast = casterPosition;
        _networkStartTime = networkStartTime;
    }

    //Führt die Fähigkeit auf Server und Clients aus.
    //Wird vom Caster unmittelbar nach InitializeAbility(...) aufgerufen
    public void ExecuteAbility()
    {
        RpcExecute();
        Execute();
    }
    [ClientRpc]
    private void RpcExecute()
    {
        Debug.Log("RpcExecute");
        Execute();
    }

    //Wird sowohl auf dem Server, als auch auf allen Clients beim Start der Fähigkeit aufgerufen
    //Muss überschrieben werden
    protected abstract void Execute();

    //Wird in jedem Frame auf dem Server ausgeführt
    //Wird in dem jeweiligen Frame NACH AllUpdate() aufgerufen
    public abstract void ServerUpdate();

    //Wird sowohl auf dem Server, als auch auf allen Clients ausgeführt
    //Wird in dem jeweiligen Frame VOR ServerUpdate() aufgerufen
    public abstract void AllUpdate();

    public void Update()
    {
        AllUpdate();
        if (isServer)
            ServerUpdate();
    }


}
