﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

using UnityEngine.Networking;
using System;

public class PlayerMovement : NetworkBehaviour {

    [Header("Movement Variables")]
    [SerializeField] public float _movementSpeedUPS = 12f;

    [Header("Camera Position Variables")]
    [SerializeField] float cameraDistance = 7f;
    [SerializeField] float cameraHeight = 15f;

    private Rigidbody localRigidbody;
    private Camera mainCamera;

    private Transform _lowerBound;
    private Transform _upperBound;

    private Vector3 movementInputValue;
    private Vector3 cameraOffset;
    private Vector3 velocity;

    private bool _isCasting;

    public bool IsJumping;

    private bool AoeCast;
    private float MaxAoeRange;

    [HideInInspector]
    public bool _canWalk;
    public bool _canRotate;
    Transform cam;
    Vector3 camForward;
    Vector3 move;
    Vector3 moveInput;

    float forwardAmount;
    float turnAmount;
    
    public void UpdateTickVariables(bool canWalk, bool canRotate, float tickMovementSpeedUPS)
    {
        _canWalk = canWalk;
        _canRotate = canRotate;
        _movementSpeedUPS = tickMovementSpeedUPS;
    }

    public void StartCast(bool aoeCast, float maxAoeRange)
    {
        _isCasting = true;
        AoeCast = aoeCast;
        if (aoeCast)
            MaxAoeRange = maxAoeRange;
    }

    public void StopCast()
    {
        _isCasting = false;
        AoeCast = false;
        MaxAoeRange = 0.0f;
        GetComponent<Caster>().Indicator.transform.position = new Vector3(transform.position.x, GetComponent<Caster>().Indicator.transform.position.y, transform.position.z);
    }

    private Plane _groundPlane;

    public Vector3 MouseWorldPosition { get; private set; }
    public Vector3 AoeWorldPosition { get; private set; }
    public Vector3 _JumpPosition;

    public override void OnStartLocalPlayer()
    {
        _canWalk = false;
        _isCasting = false;
        _canRotate = true;
        IsJumping = false;
        AoeCast = false;
        MaxAoeRange = 0.0f;
        localRigidbody = GetComponent<Rigidbody>();
        _groundPlane = new Plane(Vector3.up, Vector3.zero);
        cameraOffset = new Vector3(0f, cameraHeight, -cameraDistance);

        mainCamera = Camera.main;
        cam = Camera.main.transform;

        _lowerBound = GameObject.Find("lowerBoundEmpty").transform;
        _upperBound = GameObject.Find("upperBoundEmpty").transform;

        MoveCamera();

    }

    private void Update ()
    {
        if (!isLocalPlayer)
            return;

        if (_canWalk && !_isCasting)
        {
            movementInputValue = new Vector3(Math.Sign(CrossPlatformInputManager.GetAxisRaw("Horizontal"))*Time.deltaTime, 0.0f, Math.Sign(CrossPlatformInputManager.GetAxisRaw("Vertical"))*Time.deltaTime);
            velocity = movementInputValue.normalized * (_movementSpeedUPS * GetComponent<CharacterLoader>().Speedmultiplier);
        }
        else
            velocity = new Vector3(0, 0, 0);

        Ray cameraRay = mainCamera.ScreenPointToRay(CrossPlatformInputManager.mousePosition);
        float rayLength;

        if (_groundPlane.Raycast(cameraRay, out rayLength))
        {
            MouseWorldPosition = cameraRay.GetPoint(rayLength);
            if(_canRotate)
                transform.LookAt(new Vector3(MouseWorldPosition.x, transform.position.y, MouseWorldPosition.z));

            UpdateAoePosition();
        }

        if (!IsJumping)
            localRigidbody.velocity = velocity;
        else
        {
            transform.LookAt(new Vector3( _JumpPosition.x,transform.position.y,_JumpPosition.z));
        }

        PositionToBounds();

        MoveCamera();
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

        if (cam != null)
        {
            camForward = Vector3.Scale(cam.up, new Vector3(1, 0, 1)).normalized;
            move = vertical * camForward + horizontal * cam.right;
        }
        else
        {
            move = vertical * Vector3.forward + horizontal * Vector3.right;
        }

        if (move.magnitude >1)
        {
            move.Normalize();
        }

        Move(move);
   
    }

    private void PositionToBounds()
    {
        Vector3 curPosition = transform.position;
        curPosition.x = Mathf.Clamp(curPosition.x, _lowerBound.position.x, _upperBound.position.x);
        curPosition.z = Mathf.Clamp(curPosition.z, _lowerBound.position.z, _upperBound.position.z);
        transform.position = curPosition;
    }

    private void MoveCamera()
    {
        mainCamera.transform.position = transform.position + cameraOffset;
    }

    void Move (Vector3 move)
    {
        if (move.magnitude > 1)
        {
            move.Normalize();
        }
        this.moveInput = move;
        ConvertMoveInput();
        UpdateAnimator();
       
    }
    void ConvertMoveInput()
    {
        Vector3 localMove = transform.InverseTransformDirection(moveInput);
        turnAmount = localMove.x;

        forwardAmount = localMove.z;

    }
    void UpdateAnimator()
    {
        GetComponent<Animator>().SetFloat("BlendX", turnAmount, 0.1f, Time.deltaTime);
        GetComponent<Animator>().SetFloat("BlendY", forwardAmount, 0.1f, Time.deltaTime);
    }

    private void UpdateAoePosition()
    {
        if (!AoeCast)
            return;
        
        float distanceToSpawnPosition = (MouseWorldPosition - transform.position).magnitude;
        Vector3 direction = new Vector3(MouseWorldPosition.x - transform.position.x, 0.0f, MouseWorldPosition.z - transform.position.z).normalized;
        AoeWorldPosition = transform.position + direction * Mathf.Min(distanceToSpawnPosition, MaxAoeRange);
        GetComponent<Caster>().Indicator.transform.position = new Vector3(AoeWorldPosition.x, GetComponent<Caster>().Indicator.transform.position.y, AoeWorldPosition.z);
    }
}
